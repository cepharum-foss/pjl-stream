# pjl-stream

_extracts PJL information from streamed data_

## License

[MIT](LICENSE)

## Install

```bash
npm i pjl-stream
```

## Usage

Creating a stream parsing data passing through for PJL content is as straight-forward as this:

```javascript
import { Stream } from "pjl-stream";

const filter = new Stream();

filter.on( "pjl-segment", segment => {
    // TODO inspect segment
} );
```

You can now pipe a source into this filter and then pipe this filter into whatever drain you like:

```javascript
import { createReadStream, createWriteStream } from "fs";

const source = createReadStream( "/path/to/my/print.job" );
const drain = createWriteStream( "/path/to/my/filtered/print.job" );

source.pipe( filter ).pipe( drain );
```

For every encountered PJL segment of data passing the stream an event named `pjl` is emitted. Its payload is the parsed PJL segment basically consisting of a list of commands.

```javascript
filter.on( "pjl-segment", segment => {
    console.log( segment.commands );
} );
```

This list consists of instances of `Command`, which is the abstract base class for three types of commands:

* `NopCommand` is representing lines without any particular command. They comply with format #2  of PJL specification.
* `CommandWithWords` is representing a named command complying with format #3 of PJL specification. This is for commands `ECHO` and `COMMENT`, only.
* `CommandWithOptions` is representing a named command complying with format #4 of PJL specification. It is the common pattern for all but those commands mentioned above.

Event handlers are enabled to adjust existing commands in provided segment, remove them and add new ones. This results in extracted PJL segment of streamed data being replaced with segment resulting from any modification. Any command you keep untouched stays the way it was found in the stream originally.

### Support for broken PJL

Some printer drivers of commercial vendors do not fully comply with PJL specification. Starting with version 0.2.1, this parser accepts set of options provided on constructing stream parser. Option `strict` can be set `false` explicitly to relax the stream parser on certain violations of PJL specification.

```javascript
import { Stream } from "pjl-stream";

const filter = new Stream( { strict: false } );

filter.on( "pjl-segment", segment => {
    // TODO inspect segment
} );
```
