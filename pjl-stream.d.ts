import type { Stream as ParsingStream } from "parsing-stream";

declare module "pjl-stream" {
    /**
     * Implements stream parsing PJL code from passing data emitting
     * notification for every discovered _PJL segment_ supporting its
     * replacement in stream.
     */
    export class Stream extends ParsingStream {
        constructor(options?: StreamOptions);
    }

    export interface StreamOptions {
        /**
         * Controls whether strictly following PJL spec or not. Default is true.
         */
        strict?: boolean;
    }

    /**
     * Represents single PJL segment extracted from stream.
     */
    export class Segment {
        commands: Command[];
    }

    /**
     * Provides basic API of PJL commands.
     */
    export abstract class Command {
    }

    /**
     * Represents command without operation complying with format #2 of PJL
     * specification.
     */
    export class NopCommand extends Command {

    }

    /**
     * Represents named command with a non-empty list of words complying with
     * format #3 of PJL specification.
     */
    export class CommandWithWords extends Command {
        /**
         * Name of command in uppercase
         */
        name: string;

        /**
         * Lists words of command.
         */
        words: string[];
    }

    /**
     * Represents named command with a list of options and a modifier complying
     * with format #4 of PJL specification.
     */
    export class CommandWithOptions extends Command {
        /**
         * Name of command in uppercase
         */
        name: string;

        /**
         * Modifier adjusting this commands context.
         */
        modifier: Option;

        /**
         * Lists options of command.
         */
        options: Option[];
    }

    /**
     * Represents single modifier or option.
     */
    export class Option {
        /**
         * Name of option
         */
        name: string;

        /**
         * Value of option
         */
        value: string | number;
    }
}
