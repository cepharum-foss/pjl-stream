import { describe, it } from "mocha";
import "should";

import { Command, NopCommand, CommandWithWords, CommandWithOptions } from "../lib/commands/index.mjs";
import { Option } from "../lib/option.mjs";

describe( "NopCommand", () => {
	it( "can be created w/o arguments", () => {
		( () => new NopCommand() ).should.not.throw();
	} );

	it( "can be created w/ raw code to use for rendering in first argument", () => {
		( () => new NopCommand( "foo" ) ).should.not.throw();
	} );

	it( "is an instance of Command", () => {
		new NopCommand().should.be.instanceOf( Command );
	} );

	it( "renders as format #2 PJL command when converted to string", () => {
		String( new NopCommand() ).should.equal( "@PJL\r\n" );
	} );

	it( "renders as raw code as provided on construction when converted to string", () => {
		String( new NopCommand( "foo" ) ).should.equal( "foo" );
	} );
} );

describe( "CommandWithWords", () => {
	it( "requires three arguments provided on creation", () => {
		( () => new CommandWithWords() ).should.throw();
		( () => new CommandWithWords( "raw" ) ).should.throw();
		( () => new CommandWithWords( "raw", "echo" ) ).should.throw();

		( () => new CommandWithWords( "raw", "echo", "bar" ) ).should.not.throw();
	} );

	it( "accepts anything as raw command line in first argument of creation", () => {
		( () => new CommandWithWords( undefined, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( null, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( false, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( true, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( 0, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( -100, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( 2.3, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( [], "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( [1], "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( [true], "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( ["foo"], "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( {}, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( { name: "foo" }, "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( new Map( [[ "name", "foo" ]] ), "echo", "bar" ) ).should.not.throw();

		( () => new CommandWithWords( "raw", "echo", "bar" ) ).should.not.throw();
	} );

	it( "accepts anything stringifiable as command name as long as resulting name is valid", () => {
		// stringifying name results in invalid command name
		( () => new CommandWithWords( "raw", null, "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", undefined, "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", false, "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", true, "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", [], "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", ["foo"], "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", {}, "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", { toString: () => "foo" }, "bar" ) ).should.throw();
		( () => new CommandWithWords( "raw", () => "echo", "bar" ) ).should.throw();

		// stringifying results in valid command name
		( () => new CommandWithWords( "raw", ["comment"], "bar" ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", "bar" ) ).should.not.throw();
		( () => new CommandWithWords( "raw", { toString: () => "echo" }, "bar" ) ).should.not.throw();
	} );

	it( "accepts non-nullish stringifiable argument as list of words as long as it contains printable characters", () => {
		// nullish - rejected
		( () => new CommandWithWords( "raw", "echo", null ) ).should.throw();
		( () => new CommandWithWords( "raw", "echo", undefined ) ).should.throw();

		// non-nullish, but stringified as empty string
		( () => new CommandWithWords( "raw", "echo", [] ) ).should.throw();
		( () => new CommandWithWords( "raw", "echo", { toString: () => "" } ) ).should.throw();
		( () => new CommandWithWords( "raw", "echo", "" ) ).should.throw();

		// non-nullish, but stringified as whitespace-only string
		( () => new CommandWithWords( "raw", "echo", [" \t "] ) ).should.throw();
		( () => new CommandWithWords( "raw", "echo", { toString: () => "\t \t" } ) ).should.throw();
		( () => new CommandWithWords( "raw", "echo", "\t\t  \t" ) ).should.throw();

		// non-nullish, stringified with printable content
		( () => new CommandWithWords( "raw", "echo", true ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", false ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", 0 ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", 1 ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", ["foo bar \tbaz"] ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", { foo: "foo bar \tbaz" } ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", { toString: () => "foo bar \tbaz" } ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", () => "foo bar \tbaz" ) ).should.not.throw();
		( () => new CommandWithWords( "raw", "echo", "foo bar \tbaz" ) ).should.not.throw();
	} );

	it( "accepts change of command name after creation as long as providing stringifiable value which results in a valid name", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		// stringifying results in invalid command name
		( () => { command.name = null; } ).should.throw();
		( () => { command.name = undefined; } ).should.throw();
		( () => { command.name = false; } ).should.throw();
		( () => { command.name = true; } ).should.throw();
		( () => { command.name = []; } ).should.throw();
		( () => { command.name = ["foo"]; } ).should.throw();
		( () => { command.name = {}; } ).should.throw();
		( () => { command.name = () => "echo"; } ).should.throw();

		// stringifying results in valid command name
		( () => { command.name = ["comment"]; } ).should.not.throw();
		( () => { command.name = "echo"; } ).should.not.throw();
		( () => { command.name = { toString: () => "echo" }; } ).should.not.throw();
	} );

	it( "accepts change of command words after creation as long as providing stringifiable value which has printable characters", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		// nullish - rejected
		( () => { command.words = null; } ).should.throw();
		( () => { command.words = undefined; } ).should.throw();

		// non-nullish, but stringified as empty string
		( () => { command.words = []; } ).should.throw();
		( () => { command.words = { toString: () => "" }; } ).should.throw();
		( () => { command.words = ""; } ).should.throw();

		// non-nullish, but stringified as whitespace-only string
		( () => { command.words = [" \t "]; } ).should.throw();
		( () => { command.words = { toString: () => "\t \t" }; } ).should.throw();
		( () => { command.words = "\t\t  \t"; } ).should.throw();

		// non-nullish, stringified with printable content
		( () => { command.words = true; } ).should.not.throw();
		( () => { command.words = false; } ).should.not.throw();
		( () => { command.words = 0; } ).should.not.throw();
		( () => { command.words = 1; } ).should.not.throw();
		( () => { command.words = ["foo bar \tbaz"]; } ).should.not.throw();
		( () => { command.words = { foo: "foo bar \tbaz" }; } ).should.not.throw();
		( () => { command.words = { toString: () => "foo bar \tbaz" }; } ).should.not.throw();
		( () => { command.words = () => "foo bar \tbaz"; } ).should.not.throw();
		( () => { command.words = "foo bar \tbaz"; } ).should.not.throw();

		command.words = [ "bar", "baz", "foo" ];
		command.words.should.deepEqual( [ "bar", "baz", "foo" ] );

		command.words = "foo bar \tbaz";
		command.words.should.deepEqual( [ "foo", "bar", "baz" ] );
	} );

	it( "delivers raw version as provided on construction by default", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		command.raw.should.equal( "foo" );
	} );

	it( "delivers `false` as raw version after adjusting name", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		command.name = "comment";

		command.raw.should.be.false();
	} );

	it( "delivers raw version as provided on construction after trying to adjust name using invalid name", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		( () => { command.name = "bar"; } ).should.throw();

		command.raw.should.equal( "foo" );
	} );

	it( "delivers `false` as raw version after adjusting words", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		command.words = [ "foo", "baz" ];

		command.raw.should.be.false();
	} );

	it( "delivers raw version as provided on construction after trying to adjust words using invalid list", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		( () => { command.words = []; } ).should.throw();

		command.raw.should.equal( "foo" );
	} );

	it( "returns raw version as provided on construction when stringified by default", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		String( command ).should.equal( "foo" );
	} );

	it( "returns command rendered as format #3 PJL command when stringified after adjusting name", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		command.name = "comment";

		String( command ).should.equal( "@PJL COMMENT bar\r\n" );
	} );

	it( "returns command rendered as format #3 PJL command when stringified after adjusting words", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		command.words = [ "foo", "baz" ];

		String( command ).should.equal( "@PJL ECHO foo baz\r\n" );
	} );

	it( "returns raw version as provided on construction when stringified after trying to adjust name using invalid name", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		( () => { command.name = "bar"; } ).should.throw();

		String( command ).should.equal( "foo" );
	} );

	it( "returns raw version as provided on construction when stringified after trying to adjust words using invalid list", () => {
		const command = new CommandWithWords( "foo", "echo", "bar" );

		( () => { command.words = []; } ).should.throw();

		String( command ).should.equal( "foo" );
	} );
} );

describe( "CommandWithOptions", () => {
	it( "requires two commands provided on creation", () => {
		( () => new CommandWithOptions() ).should.throw();
		( () => new CommandWithOptions( "raw" ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo" ) ).should.not.throw();
	} );

	it( "accepts anything as raw command line in first argument of creation", () => {
		( () => new CommandWithOptions( undefined, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( null, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( false, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( true, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( 0, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( -100, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( 2.3, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( [], "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( [1], "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( [true], "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( ["foo"], "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( {}, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( { name: "foo" }, "foo" ) ).should.not.throw();
		( () => new CommandWithOptions( new Map( [[ "name", "foo" ]] ), "foo" ) ).should.not.throw();

		( () => new CommandWithOptions( "raw", "foo" ) ).should.not.throw();
	} );

	it( "accepts anything stringifiable as command name as long as resulting name is valid", () => {
		// NOK: stringifying name results in empty name
		( () => new CommandWithOptions( "raw", null ) ).should.throw();
		( () => new CommandWithOptions( "raw", undefined ) ).should.throw();

		// NOK: stringifying name results in name lacking any printable character
		( () => new CommandWithOptions( "raw", " " ) ).should.throw();
		( () => new CommandWithOptions( "raw", [] ) ).should.throw();
		( () => new CommandWithOptions( "raw", [" \t"] ) ).should.throw();
		( () => new CommandWithOptions( "raw", { toString: () => "\t \t " } ) ).should.throw();

		// NOK: stringifying name results in name containing inner whitespace
		( () => new CommandWithOptions( "raw", {} ) ).should.throw();
		( () => new CommandWithOptions( "raw", () => "echo" ) ).should.throw();
		( () => new CommandWithOptions( "raw", { toString: () => "foo bar" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo bar" ) ).should.throw();

		// OK: stringifying name results in name consisting of printable characters, only
		( () => new CommandWithOptions( "raw", false ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", true ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", ["foo"] ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", { toString: () => "foo" } ) ).should.not.throw();

		// OK: stringifying name results in name containing leading/trailing whitespace, only
		( () => new CommandWithOptions( "raw", false ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", true ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", ["foo"] ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", { toString: () => "foo" } ) ).should.not.throw();
	} );

	it( "rejects anything stringifying to reserved name of PJL format #3 commands as command name", () => {
		( () => new CommandWithOptions( "raw", " echo " ) ).should.throw();
		( () => new CommandWithOptions( "raw", ["EcHo"] ) ).should.throw();
		( () => new CommandWithOptions( "raw", { toString: () => "\t EchO \t " } ) ).should.throw();

		( () => new CommandWithOptions( "raw", " COMMENT " ) ).should.throw();
		( () => new CommandWithOptions( "raw", ["comment"] ) ).should.throw();
		( () => new CommandWithOptions( "raw", { toString: () => "\t cOmMeNT \t " } ) ).should.throw();
	} );

	it( "accepts nullish modifier in third argument", () => {
		( () => new CommandWithOptions( "raw", "foo", null ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", "foo", undefined ) ).should.not.throw();
	} );

	it( "accepts non-nullish modifier in third argument which is an instance of Option describing name w/o value", () => {
		( () => new CommandWithOptions( "raw", "foo", new Option( "bar" ) ) ).should.not.throw();
	} );

	it( "accepts non-nullish modifier in third argument which is an instance of Option describing name and value", () => {
		( () => new CommandWithOptions( "raw", "foo", new Option( "bar", "baz" ) ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", "foo", new Option( "bar", "1.23" ) ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", "foo", new Option( "bar", "baz bang" ) ) ).should.not.throw();
	} );

	it( "rejects non-nullish modifier in third argument which is not an instance of Option", () => {
		// non-nullish, but stringified as empty string
		( () => new CommandWithOptions( "raw", "foo", [] ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", { toString: () => "" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", "" ) ).should.throw();

		// non-nullish, but stringified as whitespace-only string
		( () => new CommandWithOptions( "raw", "foo", [" \t "] ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", { toString: () => "\t \t" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", "\t\t  \t" ) ).should.throw();

		// non-nullish, stringified with printable content
		( () => new CommandWithOptions( "raw", "foo", true ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", false ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", 0 ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", 1 ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", ["foo bar \tbaz"] ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", { foo: "foo bar \tbaz" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", { toString: () => "foo bar \tbaz" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", () => "foo bar \tbaz" ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", "foo bar \tbaz" ) ).should.throw();
	} );

	it( "accepts nullish value as list of options in fourth argument", () => {
		( () => new CommandWithOptions( "raw", "foo", null, null ) ).should.not.throw();
		( () => new CommandWithOptions( "raw", "foo", null, undefined ) ).should.not.throw();
	} );

	it( "accepts empty list of options in fourth argument", () => {
		( () => new CommandWithOptions( "raw", "foo", null, [] ) ).should.not.throw();
	} );

	it( "accepts list of options in fourth argument which consists of instances of Option, only", () => {
		const factory = () => [
			new Option( "bar" ),
			new Option( "bar", "baz" ),
			new Option( "bar", "1.23" ),
			new Option( "bar", "baz bang" ),
		];

		const options = [
			...factory(),
			...factory(),
			...factory(),
			...factory(),
			...factory(),
			...factory(),
			...factory(),
		];

		( () => new CommandWithOptions( "raw", "foo", null, options ) ).should.not.throw();
	} );

	it( "rejects list of options in fourth argument as soon as it contains a value which is no instance of Option", () => {
		const factory = () => [
			new Option( "bar" ),
			new Option( "bar", "baz" ),
			new Option( "bar", "1.23" ),
			new Option( "bar", "baz bang" ),
		];

		const options = [
			...factory(),
			...factory(),
			...factory(),
			...factory(),
			...factory(),
			...factory(),
			...factory(),
		];

		for ( let count = 1; count < 10; count++ ) {
			const injectedCopy = ( index, value ) => {
				const copy = [...options];

				copy.splice( index, count, ...new Array( count ).fill( 0 ).map( () => value ) );

				return copy;
			};

			for ( let index = 0; index < options.length - count; index++ ) {
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, null ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, undefined ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, true ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, false ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, "" ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, [] ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, ["foo"] ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, {} ) ) ).should.throw();
				( () => new CommandWithOptions( "raw", "foo", null, injectedCopy( index, { toString: () => "foo" } ) ) ).should.throw();
			}
		}
	} );

	it( "rejects non-nullish non-array value as list of options in fourth argument", () => {
		( () => new CommandWithOptions( "raw", "foo", null, { toString: () => "" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, "" ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, { toString: () => "\t \t" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, "\t\t  \t" ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, true ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, false ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, 0 ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, 1 ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, { foo: "foo bar \tbaz" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, { toString: () => "foo bar \tbaz" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, { name: "baz", value: "bang" } ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, () => "foo bar \tbaz" ) ).should.throw();
		( () => new CommandWithOptions( "raw", "foo", null, "foo bar \tbaz" ) ).should.throw();
	} );

	it( "accepts change of command name after creation as long as providing stringifiable value which results in a valid name", () => {
		const command = new CommandWithOptions( "foo", "whatever" );

		// stringifying results in invalid command name
		( () => { command.name = null; } ).should.throw();
		( () => { command.name = undefined; } ).should.throw();
		( () => { command.name = []; } ).should.throw();
		( () => { command.name = {}; } ).should.throw();
		( () => { command.name = () => "echo"; } ).should.throw();

		// stringifying results in valid command name
		( () => { command.name = false; } ).should.not.throw();
		( () => { command.name = true; } ).should.not.throw();
		( () => { command.name = ["foo"]; } ).should.not.throw();
		( () => { command.name = ["maeh"]; } ).should.not.throw();
		( () => { command.name = "moo"; } ).should.not.throw();
		( () => { command.name = { toString: () => "moo" }; } ).should.not.throw();
	} );

	it( "accepts change of modifier after creation as long as providing nullish value or instance of Option", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		// nullish - accepted
		( () => { command.modifier = null; } ).should.not.throw();
		( () => { command.modifier = undefined; } ).should.not.throw();

		// non-nullish, but stringified as empty string
		( () => { command.modifier = []; } ).should.throw();
		( () => { command.modifier = { toString: () => "" }; } ).should.throw();
		( () => { command.modifier = ""; } ).should.throw();

		// non-nullish, but stringified as whitespace-only string
		( () => { command.modifier = [" \t "]; } ).should.throw();
		( () => { command.modifier = { toString: () => "\t \t" }; } ).should.throw();
		( () => { command.modifier = "\t\t  \t"; } ).should.throw();

		// non-nullish, stringified with printable content
		( () => { command.modifier = true; } ).should.throw();
		( () => { command.modifier = false; } ).should.throw();
		( () => { command.modifier = 0; } ).should.throw();
		( () => { command.modifier = 1; } ).should.throw();
		( () => { command.modifier = ["foo bar \tbaz"]; } ).should.throw();
		( () => { command.modifier = { foo: "foo bar \tbaz" }; } ).should.throw();
		( () => { command.modifier = { toString: () => "foo bar \tbaz" }; } ).should.throw();
		( () => { command.modifier = () => "foo bar \tbaz"; } ).should.throw();
		( () => { command.modifier = () => new Option( "baz" ); } ).should.throw();
		( () => { command.modifier = "foo bar \tbaz"; } ).should.throw();
		( () => { command.modifier = { name: "baz", value: "bang" }; } ).should.throw();

		// non-nullish, instance of Option
		( () => { command.modifier = new Option( "baz" ); } ).should.not.throw();
		( () => { command.modifier = new Option( "baz", "bang" ); } ).should.not.throw();

		command.modifier = new Option( "bam" );
		command.modifier.name.should.equal( "bam" );
		( command.modifier.value == null ).should.be.true();

		command.modifier = new Option( "bam", "bang" );
		command.modifier.name.should.equal( "bam" );
		command.modifier.value.should.equal( "bang" );

		command.modifier = null;
		( command.modifier == null ).should.be.true();
		( () => command.modifier.name ).should.throw();
		( () => command.modifier.value ).should.throw();
	} );

	it( "always rejects to replace list of options after creation", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		// nullish
		( () => { command.options = null; } ).should.throw();
		( () => { command.options = undefined; } ).should.throw();

		// non-nullish, but stringified as empty string
		( () => { command.options = []; } ).should.throw();
		( () => { command.options = { toString: () => "" }; } ).should.throw();
		( () => { command.options = ""; } ).should.throw();

		// non-nullish, but stringified as whitespace-only string
		( () => { command.options = [" \t "]; } ).should.throw();
		( () => { command.options = { toString: () => "\t \t" }; } ).should.throw();
		( () => { command.options = "\t\t  \t"; } ).should.throw();

		// non-nullish, stringified with printable content
		( () => { command.options = true; } ).should.throw();
		( () => { command.options = false; } ).should.throw();
		( () => { command.options = 0; } ).should.throw();
		( () => { command.options = 1; } ).should.throw();
		( () => { command.options = ["foo bar \tbaz"]; } ).should.throw();
		( () => { command.options = { foo: "foo bar \tbaz" }; } ).should.throw();
		( () => { command.options = { toString: () => "foo bar \tbaz" }; } ).should.throw();
		( () => { command.options = () => "foo bar \tbaz"; } ).should.throw();
		( () => { command.options = () => new Option( "baz" ); } ).should.throw();
		( () => { command.options = "foo bar \tbaz"; } ).should.throw();
		( () => { command.options = { name: "baz", value: "bang" }; } ).should.throw();

		// non-nullish, list of instances of Option
		( () => { command.options = [new Option( "baz" )]; } ).should.throw();
		( () => { command.options = [new Option( "baz", "bang" )]; } ).should.throw();

		command.raw.should.not.be.false();
	} );

	it( "always exposes list of options", () => {
		const command = new CommandWithOptions( "foo", "name" );
		command.options.should.be.Array().which.is.empty();

		const command2 = new CommandWithOptions( "foo", "name", null, [new Option( "baz" )] );
		command2.options.should.be.Array().which.has.length( 1 );
		command2.options.every( option => option instanceof Option ).should.be.true();
	} );

	it( "accepts instances of Option pushed to existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.push( new Option( "bar" ) ) ).should.not.throw();
		( () => command.options.push( new Option( "bar", "baz" ) ) ).should.not.throw();

		command.options.should.have.length( 2 );
		command.options.every( option => option instanceof Option ).should.be.true();

		command.raw.should.be.false();
	} );

	it( "rejects non-Option values being pushed to existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.push( undefined ) ).should.throw();
		( () => command.options.push( null ) ).should.throw();
		( () => command.options.push( false ) ).should.throw();
		( () => command.options.push( true ) ).should.throw();
		( () => command.options.push( 0 ) ).should.throw();
		( () => command.options.push( 1 ) ).should.throw();
		( () => command.options.push( -1.2 ) ).should.throw();
		( () => command.options.push( [] ) ).should.throw();
		( () => command.options.push( [new Option( "bar" )] ) ).should.throw();
		( () => command.options.push( {} ) ).should.throw();
		( () => command.options.push( { name: "bar", value: "baz" } ) ).should.throw();
		( () => command.options.push( () => new Option( "bar" ) ) ).should.throw();

		command.options.should.be.empty();
		command.raw.should.not.be.false();
	} );

	it( "accepts instances of Option unshifted to existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.unshift( new Option( "bar" ) ) ).should.not.throw();
		( () => command.options.unshift( new Option( "bar", "baz" ) ) ).should.not.throw();

		command.options.should.have.length( 2 );
		command.options.every( option => option instanceof Option ).should.be.true();
		command.raw.should.be.false();
	} );

	it( "rejects non-Option values being unshifted to existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.unshift( undefined ) ).should.throw();
		( () => command.options.unshift( null ) ).should.throw();
		( () => command.options.unshift( false ) ).should.throw();
		( () => command.options.unshift( true ) ).should.throw();
		( () => command.options.unshift( 0 ) ).should.throw();
		( () => command.options.unshift( 1 ) ).should.throw();
		( () => command.options.unshift( -1.2 ) ).should.throw();
		( () => command.options.unshift( [] ) ).should.throw();
		( () => command.options.unshift( [new Option( "bar" )] ) ).should.throw();
		( () => command.options.unshift( {} ) ).should.throw();
		( () => command.options.unshift( { name: "bar", value: "baz" } ) ).should.throw();
		( () => command.options.unshift( () => new Option( "bar" ) ) ).should.throw();

		command.options.should.be.empty();
		command.raw.should.not.be.false();
	} );

	it( "accepts instances of Option replacing items of existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.splice( 100, 0, new Option( "bar" ) ) ).should.not.throw();
		( () => command.options.splice( 1, 200, new Option( "bar", "baz" ), new Option( "foo" ) ) ).should.not.throw();

		command.options.should.have.length( 3 );
		command.options.every( option => option instanceof Option ).should.be.true();
		command.raw.should.be.false();
	} );

	it( "rejects non-Option values replacing items of existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.splice( 0, 100, undefined ) ).should.throw();
		( () => command.options.splice( 0, 100, null ) ).should.throw();
		( () => command.options.splice( 0, 100, false ) ).should.throw();
		( () => command.options.splice( 0, 100, true ) ).should.throw();
		( () => command.options.splice( 0, 100, 0 ) ).should.throw();
		( () => command.options.splice( 0, 100, 1 ) ).should.throw();
		( () => command.options.splice( 0, 100, -1.2 ) ).should.throw();
		( () => command.options.splice( 0, 100, [] ) ).should.throw();
		( () => command.options.splice( 0, 100, [new Option( "bar" )] ) ).should.throw();
		( () => command.options.splice( 0, 100, {} ) ).should.throw();
		( () => command.options.splice( 0, 100, { name: "bar", value: "baz" } ) ).should.throw();
		( () => command.options.splice( 0, 100, () => new Option( "bar" ) ) ).should.throw();

		command.options.should.be.empty();
		command.raw.should.not.be.false();
	} );

	it( "accepts instances of Option assigned to existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => { command.options[0] = new Option( "bar" ); } ).should.not.throw();
		( () => { command.options[1] = new Option( "bar", "baz" ); } ).should.not.throw();

		command.options.should.have.length( 2 );
		command.options.every( option => option instanceof Option ).should.be.true();
		command.raw.should.be.false();
	} );

	it( "rejects non-Option values being assigned to existing list of options", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => { command.options[0] = undefined; } ).should.throw();
		( () => { command.options[0] = null; } ).should.throw();
		( () => { command.options[0] = false; } ).should.throw();
		( () => { command.options[0] = true; } ).should.throw();
		( () => { command.options[0] = 0; } ).should.throw();
		( () => { command.options[0] = 1; } ).should.throw();
		( () => { command.options[0] = -1.2; } ).should.throw();
		( () => { command.options[0] = []; } ).should.throw();
		( () => { command.options[0] = [new Option( "bar" )]; } ).should.throw();
		( () => { command.options[0] = {}; } ).should.throw();
		( () => { command.options[0] = { name: "bar", value: "baz" }; } ).should.throw();
		( () => { command.options[0] = () => new Option( "bar" ); } ).should.throw();

		command.options.should.be.empty();
		command.raw.should.not.be.false();
	} );

	it( "rejects assignment of Option causing list of options to contain sparse items", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => { command.options[100] = new Option( "bar" ); } ).should.throw();
		( () => { command.options[1] = new Option( "bar", "baz" ); } ).should.throw();

		command.options.should.be.empty();
		command.raw.should.not.be.false();
	} );

	it( "detects actual change of listed options on shifting", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.shift() ).should.not.throw();

		command.raw.should.not.be.false();

		const command2 = new CommandWithOptions( "foo", "moo", null, [new Option( "bar" )] );

		( () => command2.options.shift() ).should.not.throw();

		command2.raw.should.be.false();
	} );

	it( "detects actual change of listed options on popping", () => {
		const command = new CommandWithOptions( "foo", "moo" );

		( () => command.options.pop() ).should.not.throw();

		command.raw.should.not.be.false();

		const command2 = new CommandWithOptions( "foo", "moo", null, [new Option( "bar" )] );

		( () => command2.options.pop() ).should.not.throw();

		command2.raw.should.be.false();
	} );

	it( "delivers raw version as provided on construction by default", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		command.raw.should.equal( "foo" );
	} );

	it( "delivers `false` as raw version after actually adjusting name", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		command.name = "bar";
		command.raw.should.be.equal( "foo" );

		command.name = "baz";
		command.raw.should.be.false();
	} );

	it( "delivers `false` as raw version after actually adjusting modifier", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		command.modifier = null; // no actual change
		command.raw.should.be.equal( "foo" );

		command.modifier = new Option( "baz" );
		command.raw.should.be.false();

		const command2 = new CommandWithOptions( "foo", "bar", new Option( "baz" ) );

		command2.modifier = new Option( "baz" );
		command2.raw.should.be.false();

		const command3 = new CommandWithOptions( "foo", "bar", new Option( "baz" ) );

		command3.modifier.name = "baz"; // no actual change
		command3.raw.should.equal( "foo" );

		command3.modifier.value = null; // no actual change
		command3.raw.should.equal( "foo" );

		command3.modifier.name = "bar";
		command3.raw.should.be.false();
	} );

	it( "delivers `false` as raw version after actually adjusting options", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		command.options.shift(); // no actual change
		command.raw.should.be.equal( "foo" );

		command.options.push( new Option( "baz" ) );
		command.raw.should.be.false();

		const command2 = new CommandWithOptions( "foo", "bar", null, [new Option( "baz" )] );

		command2.options[0] = new Option( "baz" );
		command2.raw.should.be.false();

		const command3 = new CommandWithOptions( "foo", "bar", null, [new Option( "baz" )] );

		command3.options[0].name = "baz"; // no actual change
		command3.raw.should.equal( "foo" );

		command3.options[0].value = null; // no actual change
		command3.raw.should.equal( "foo" );

		command3.options[0].name = "bar";
		command3.raw.should.be.false();
	} );

	it( "delivers raw version as provided on construction after trying to adjust name using invalid name", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		( () => { command.name = "bar baz"; } ).should.throw();

		command.raw.should.equal( "foo" );
	} );

	it( "delivers raw version as provided on construction after trying to adjust modifiers using anything non-nullish but an instance of Option", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		( () => { command.modifier = false; } ).should.throw();
		command.raw.should.equal( "foo" );

		( () => { command.modifier = true; } ).should.throw();
		command.raw.should.equal( "foo" );

		( () => { command.modifier = []; } ).should.throw();
		command.raw.should.equal( "foo" );

		( () => { command.modifier = {}; } ).should.throw();
		command.raw.should.equal( "foo" );

		( () => { command.modifier = { name: "baz", value: "bang" }; } ).should.throw();
		command.raw.should.equal( "foo" );

		( () => { command.modifier = () => new Option( "baz" ); } ).should.throw();
		command.raw.should.equal( "foo" );
	} );

	it( "returns raw version as provided on construction when stringified by default", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		String( command ).should.equal( "foo" );
	} );

	it( "returns command rendered as format #4 PJL command when stringified after actually adjusting name", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		command.name = "bar";
		String( command ).should.equal( "foo" );

		command.name = "foo";
		String( command ).should.equal( "@PJL FOO\r\n" );
	} );

	it( "returns command rendered as format #4 PJL command when stringified after actually adjusting modifier", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		command.modifier = null;
		String( command ).should.equal( "foo" );

		command.modifier = new Option( "baz", "bang" );
		String( command ).should.equal( "@PJL BAR baz:bang\r\n" );

		const command2 = new CommandWithOptions( "foo", "bar", new Option( "baz" ) );

		command2.modifier.name = "baz";
		String( command2 ).should.equal( "foo" );

		command2.modifier.value = null;
		String( command2 ).should.equal( "foo" );

		command2.modifier = new Option( "baz" );
		String( command2 ).should.equal( "@PJL BAR baz\r\n" );
	} );

	it( "returns command rendered as format #4 PJL command when stringified after actually adjusting options", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		command.options.shift();
		String( command ).should.equal( "foo" );

		command.options.unshift( new Option( "baz", "bang foo" ), new Option( "zip", "zap" ) );
		String( command ).should.equal( '@PJL BAR baz="bang foo" zip=zap\r\n' );

		const command2 = new CommandWithOptions( "foo", "bar", null, [new Option( "baz" )] );

		command2.options[0] = new Option( "baz" );
		String( command2 ).should.equal( "@PJL BAR baz\r\n" );
	} );

	it( "returns raw version as provided on construction when stringified after trying to adjust name using invalid name", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		( () => { command.name = "bar baz"; } ).should.throw();

		String( command ).should.equal( "foo" );
	} );

	it( "returns raw version as provided on construction when stringified after trying to adjust modifier using anything non-nullish but an instance of Option", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		( () => { command.modifier = false; } ).should.throw();
		String( command ).should.equal( "foo" );

		( () => { command.modifier = true; } ).should.throw();
		String( command ).should.equal( "foo" );

		( () => { command.modifier = []; } ).should.throw();
		String( command ).should.equal( "foo" );

		( () => { command.modifier = {}; } ).should.throw();
		String( command ).should.equal( "foo" );

		( () => { command.modifier = { name: "baz", value: "bang" }; } ).should.throw();
		String( command ).should.equal( "foo" );

		( () => { command.modifier = () => new Option( "baz" ); } ).should.throw();
		String( command ).should.equal( "foo" );
	} );

	it( "returns raw version as provided on construction when stringified after trying to adjust options using anything non-nullish but an instance of Option", () => {
		const command = new CommandWithOptions( "foo", "bar" );

		( () => command.options.push( false ) ).should.throw();
		String( command ).should.equal( "foo" );

		( () => command.options.push( true ) ).should.throw();
		String( command ).should.equal( "foo" );

		( () => command.options.push( [] ) ).should.throw();
		String( command ).should.equal( "foo" );

		( () => command.options.push( {} ) ).should.throw();
		String( command ).should.equal( "foo" );

		( () => command.options.push( { name: "baz", value: "bang" } ) ).should.throw();
		String( command ).should.equal( "foo" );

		( () => command.options.push( () => new Option( "baz" ) ) ).should.throw();
		String( command ).should.equal( "foo" );
	} );
} );
