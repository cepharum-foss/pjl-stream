import { randomBytes } from "crypto";

import { describe, it } from "mocha";
import { BufferStream } from "parsing-stream/index.mjs";
import "should";

import { UEL } from "../lib/basics.mjs";
import { toBuffer } from "../lib/roman-9.mjs";
import { Stream } from "../lib/stream.mjs";

describe( "PJL-parsing Stream", () => {
	it( "is exposed", () => {
		Stream.should.be.ok();
	} );

	it( "can be created without arguments", () => {
		( () => new Stream() ).should.not.throw();
	} );

	it( "can be used to discover PJL segment ending with ENTER LANGUAGE-command in a stream of binary data", async() => {
		const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y \tWATERMARK\t=\t"helLO world!"\r\n@PJL ENTER LANGUAGE=PCL\r\n`;

		for ( const offset of [ 0, Infinity, 1, 2, 3, -1, -1, -1, -1, -1, -1, -1 ] ) {
			const buffer = getDataWithEmbeddedPJL( 65536, code, false, offset );

			const segments = [];
			const data = await process( buffer, segments ); // eslint-disable-line no-await-in-loop

			data.should.deepEqual( buffer );

			segments.should.have.length( 1 );
			String( segments[0] ).should.equal( code );
		}
	} );

	it( "can be used to discover PJL segment ending with another UEL in a stream of binary data", async() => {
		const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y \tWATERMARK\t=\t"helLO world!"\r\n`;

		for ( const offset of [ 0, Infinity, 1, 2, 3, -1, -1, -1, -1, -1, -1, -1 ] ) {
			const buffer = getDataWithEmbeddedPJL( 65536, code, true, offset );

			const segments = [];
			const data = await process( buffer, segments ); // eslint-disable-line no-await-in-loop

			data.should.deepEqual( buffer );

			segments.should.have.length( 1 );
			String( segments[0] ).should.equal( code );
		}
	} );

	it( "can be used to discover multiple PJL fragments ending with ENTER LANGUAGE-command in a stream of binary data", async() => {
		const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y \tWATERMARK\t=\t"helLO world!"\r\n@PJL ENTER LANGUAGE=PCL\r\n`;

		for ( const offset of [ 0, Infinity, 1, 2, 3, -1, -1, -1, -1, -1, -1, -1 ] ) {
			const buffer = getDataWithEmbeddedPJL( 65536, code, false, offset, offset );

			const segments = [];
			const data = await process( buffer, segments ); // eslint-disable-line no-await-in-loop

			data.should.deepEqual( buffer );

			segments.should.have.length( 2 );
			String( segments[0] ).should.equal( code );
			String( segments[1] ).should.equal( code );
			segments[0].should.not.equal( segments[1] );
		}
	} );

	it( "can be used to discover multiple PJL fragments ending with another UEL in a stream of binary data", async() => {
		const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y \tWATERMARK\t=\t"helLO world!"\r\n`;

		for ( const offset of [ 0, Infinity, 1, 2, 3, -1, -1, -1, -1, -1, -1, -1 ] ) {
			const buffer = getDataWithEmbeddedPJL( 65536, code, true, offset, offset );

			const segments = [];
			const data = await process( buffer, segments ); // eslint-disable-line no-await-in-loop

			data.should.deepEqual( buffer );

			segments.should.have.length( 2 );
			String( segments[0] ).should.equal( code );
			String( segments[1] ).should.equal( code );
			segments[0].should.not.equal( segments[1] );
		}
	} );

	it( "can be used to adjust PJL fragments ending with ENTER LANGUAGE-command in a stream of binary data", async() => {
		const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y \tWATERMARK\t=\t"helLO world!"\r\n@PJL ENTER LANGUAGE=PCL\r\n`;

		for ( const offset of [ 0, Infinity, 1, 2, 3, -1, -1, -1, -1, -1, -1, -1 ] ) {
			const buffer = getDataWithEmbeddedPJL( 65536, code, false, offset );

			const data = await process( buffer, null, segment => { // eslint-disable-line no-await-in-loop
				segment.commands[2].options[0].value = "n"; // eslint-disable-line no-param-reassign
			} );

			data.should.not.deepEqual( buffer );
			data.toString().includes( '@PJL SET LANGUAGE:PCL PUNCH=n WATERMARK="helLO world!"\r\n' ).should.be.true();
		}
	} );

	it( "can be used to adjust PJL fragments ending with another UEL in a stream of binary data", async() => {
		const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y \tWATERMARK\t=\t"helLO world!"\r\n`;

		for ( const offset of [ 0, Infinity, 1, 2, 3, -1, -1, -1, -1, -1, -1, -1 ] ) {
			const buffer = getDataWithEmbeddedPJL( 65536, code, true, offset );

			const data = await process( buffer, null, segment => { // eslint-disable-line no-await-in-loop
				segment.commands[2].options[0].value = "n"; // eslint-disable-line no-param-reassign
			} );

			data.should.not.deepEqual( buffer );
			data.toString().includes( '@PJL SET LANGUAGE:PCL PUNCH=n WATERMARK="helLO world!"\r\n' ).should.be.true();
		}
	} );

	it( "can be used to completely remove PJL segment ending with ENTER LANGUAGE-command from a stream of binary data", async() => {
		const code = `\x00${UEL}@PJL\r\n@PJL COMMENT foo bar \r\n@PJL SET LANGUAGE:PCL \tWATERMARK\t=\t"helLO world!"\r\n@PJL ENTER LANGUAGE=PCL\r\n\xff`;

		const data = await process( Buffer.from( code ), null, segment => { // eslint-disable-line no-await-in-loop
			segment.commands.splice( 0, 4 );
		} );

		data.toString().should.equal( "\x00\xff" );
	} );

	it( "can be used to completely remove PJL segment ending with another UEL from a stream of binary data", async() => {
		const code = `\x00${UEL}@PJL\r\n@PJL COMMENT foo bar \r\n@PJL SET LANGUAGE:PCL \tWATERMARK\t=\t"helLO world!"\r\n${UEL}\xff`;

		const data = await process( Buffer.from( code ), null, segment => { // eslint-disable-line no-await-in-loop
			segment.commands.splice( 0, 4 );
		} );

		data.toString().should.equal( "\x00\xff" );
	} );

	it( "fails on incomplete PJL segment lacking ENTER LANGUAGE-command and another UEL found in a stream of binary data", async() => {
		const code = `\x00${UEL}@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL \tWATERMARK\t=\t"helLO world!"\r\n\xff`;

		await process( Buffer.from( code ), null, segment => { // eslint-disable-line no-await-in-loop
			segment.commands.splice( 0, 4 );
		} ).should.be.rejectedWith( /incomplete\s+pjl/i );
	} );

	it( "processes stream that does not contain any PJL", async() => {
		const buffer = randomBytes( 65536 );
		const segments = [];
		const data = await process( buffer, segments ); // eslint-disable-line no-await-in-loop

		data.should.deepEqual( buffer );

		segments.should.have.length( 0 );
	} );
} );

/**
 * Generates random binary data and embeds provided PJL code at selected or
 * random offset.
 *
 * @param {number} size number of octets in resulting buffer
 * @param {string} pjl code of PJL segment to embed
 * @param {boolean} complete set false to embed PJL without closing UEL
 * @param {number[]} offsets indices into buffer for first byte containing embedded PJL segments (each preceded by UEL)
 * @returns {Buffer} buffer with binary data and provided PJL segment
 */
function getDataWithEmbeddedPJL( size, pjl, complete, ...offsets ) {
	const buffer = randomBytes( size );
	const sub = toBuffer( `${UEL}${pjl}${complete ? UEL : ""}` );
	const limit = Math.floor( size / offsets.length );
	const max = limit - sub.length;

	for ( let i = 0; i < offsets.length; i++ ) {
		const offset = offsets[i];
		const start = ( limit * i ) + ( offset > -1 ? Math.min( max, offset ) : Math.floor( Math.random() * max ) );

		sub.copy( buffer, start );
	}

	return buffer;
}

/**
 * Processes provided buffer looking for containing PJL segments to be collected
 * in provided collector.
 *
 * @param {Buffer} buffer buffer to parse for contained PJL
 * @param {Segment[]} segments reported segments extracted from buffer
 * @param {function} handler callback to invoke explicitly for handling discovered PJL segments
 * @returns {Promise<Buffer>} promises all passing data
 */
async function process( buffer, segments, handler = null ) {
	const source = new BufferStream.Reader( buffer );
	const filter = new Stream();
	const sink = new BufferStream.Writer();

	filter.on( "pjl-segment", handler || ( segment => {
		segments.push( segment );
	} ) );

	source.pipe( filter ).pipe( sink );
	source.once( "error", cause => filter.destroy( cause ) );
	filter.once( "error", cause => sink.destroy( cause ) );

	return await sink.asPromise;
}
