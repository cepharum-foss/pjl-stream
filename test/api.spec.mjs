import { Transform } from "stream";

import { describe, it } from "mocha";
import "should";

import * as API from "../index.mjs";


describe( "PJL exposes Stream class which", function() {
	it( "is a transform stream", function() {
		( API.Stream != null ).should.be.true();

		new API.Stream().should.be.instanceOf( Transform );
	} );

	it( "can be constructed without arguments", function() {
		( () => new API.Stream() ).should.not.throw();
	} );
} );
