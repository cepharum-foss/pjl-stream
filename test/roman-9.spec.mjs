import { describe, it } from "mocha";
import "should";

import { fromBuffer, fromOctet, toBuffer, toOctet } from "../lib/roman-9.mjs";

const supportedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"$%&/()=?*+'#-_.:,;<>´`^°[]{}äöüÄÖÜß€áéóíÁÉÓÍàèòìÀÈÌÒ";

describe( "HP Roman-9 encoding", () => {
	describe( "is supported via encoding maps", () => {
		describe( "including map for converting octets into related UTF-8 character which", () => {
			it( "is an instance of Map", () => {
				fromOctet.should.be.instanceOf( Map );
			} );

			it( "has 256 entries", () => {
				fromOctet.size.should.equal( 256 );
			} );

			it( "transcodes undefined octets 0x80 - 0x9f and 0xff to private unicode codepoints", () => {
				for ( let i = 0x80; i < 0xa0; i++ ) {
					fromOctet.get( i ).should.equal( String.fromCodePoint( 0xe000 + i ) );
				}

				fromOctet.get( 0xff ).should.equal( String.fromCodePoint( 0xe0ff ) );
			} );
		} );

		describe( "including map for converting UTF-8 character into octet which", () => {
			it( "is an instance of Map", () => {
				toOctet.should.be.instanceOf( Map );
			} );

			it( "has 256 entries", () => {
				toOctet.size.should.equal( 256 );
			} );

			it( "transcodes off-range codepoints to `undefined`", () => {
				for ( let i = 0x100; i < 0xe000; i++ ) {
					if ( fromOctet.get( i ) !== undefined ) {
						( toOctet.get( String.fromCodePoint( i ) ) === undefined )
							.should.be.true( `codepoint 0x${i.toString( 16 )} not transcoded to undefined` );
					}
				}

				for ( let i = 0xe100; i < 0x20000; i++ ) {
					( toOctet.get( String.fromCodePoint( i ) ) === undefined ).should.be.true();
				}
			} );
		} );

		describe( "which - when combined -", () => {
			for ( const character of supportedCharacters ) {
				it( `safely transcode supported character "${character}" to/from octet`, () => {
					const octet = toOctet.get( character );

					octet.should.be.Number().which.is.greaterThanOrEqual( 0 ).and.lessThanOrEqual( 255 );

					fromOctet.get( octet ).should.equal( character );
				} );
			}

			it( "transcodes every octet to/from character", () => {
				for ( let i = 0; i < 256; i++ ) {
					toOctet.get( fromOctet.get( i ) ).should.equal( i );
				}
			} );
		} );
	} );

	describe( "is supported via transcoding helpers", () => {
		describe( "including method toBuffer() which", () => {
			it( "is a function", () => {
				toBuffer.should.be.a.Function();
			} );

			it( "requires string as input", () => {
				( () => toBuffer() ).should.throw();
				( () => toBuffer( true ) ).should.throw();
				( () => toBuffer( false ) ).should.throw();
				( () => toBuffer( null ) ).should.throw();
				( () => toBuffer( undefined ) ).should.throw();
				( () => toBuffer( 0 ) ).should.throw();
				( () => toBuffer( 20 ) ).should.throw();
				( () => toBuffer( [] ) ).should.throw();
				( () => toBuffer( ["foo"] ) ).should.throw();
				( () => toBuffer( {} ) ).should.throw();
				( () => toBuffer( { foo: "bar" } ) ).should.throw();
				( () => toBuffer( () => "foo" ) ).should.throw();
				( () => toBuffer( Buffer.from( "foo" ) ) ).should.throw();

				( () => toBuffer( "foo" ) ).should.not.throw();
			} );

			it( "converts string to Buffer instance", () => {
				toBuffer( "foo" ).should.be.instanceOf( Buffer ).which.has.length( 3 );
			} );

			it( "can convert empty string", () => {
				toBuffer( "" ).should.be.instanceOf( Buffer ).which.is.empty();
			} );

			it( "converts unknown characters of string to 0xff octet", () => {
				toBuffer( "AחB" ).should.be.instanceOf( Buffer ).which.is.deepEqual( Buffer.from( [ 0x41, 0xff, 0x42 ] ) );
			} );
		} );

		describe( "including method fromBuffer() which", () => {
			it( "is a function", () => {
				fromBuffer.should.be.a.Function();
			} );

			it( "requires Buffer as input", () => {
				( () => fromBuffer() ).should.throw();
				( () => fromBuffer( true ) ).should.throw();
				( () => fromBuffer( false ) ).should.throw();
				( () => fromBuffer( null ) ).should.throw();
				( () => fromBuffer( undefined ) ).should.throw();
				( () => fromBuffer( 0 ) ).should.throw();
				( () => fromBuffer( 20 ) ).should.throw();
				( () => fromBuffer( [] ) ).should.throw();
				( () => fromBuffer( ["foo"] ) ).should.throw();
				( () => fromBuffer( {} ) ).should.throw();
				( () => fromBuffer( { foo: "bar" } ) ).should.throw();
				( () => fromBuffer( () => "foo" ) ).should.throw();
				( () => fromBuffer( "foo" ) ).should.throw();

				( () => fromBuffer( Buffer.from( "foo" ) ) ).should.not.throw();
			} );

			it( "converts Buffer to string", () => {
				fromBuffer( Buffer.from( "foo" ) ).should.be.String().which.has.length( 3 );
			} );

			it( "can convert empty string", () => {
				fromBuffer( Buffer.alloc( 0 ) ).should.equal( "" );
			} );
		} );

		describe( "which can be combined to", () => {
			for ( const character of supportedCharacters ) {
				it( `transcode string "${character}" to/from Buffer`, () => {
					fromBuffer( toBuffer( character ) ).should.equal( character );
				} );
			}


			it( "transcode every octet to/from character", () => {
				for ( let i = 0; i < 256; i++ ) {
					const buffer = Buffer.from( [i] );

					toBuffer( fromBuffer( buffer ) ).should.deepEqual( buffer );
				}
			} );
		} );
	} );
} );
