import { describe, it } from "mocha";
import { BufferStream } from "parsing-stream/index.mjs";
import "should";

import { Stream } from "../lib/stream.mjs";

describe( "Parsing examples of PJL specification document", () => {
	it( "page #34 top works", async() => {
		const code = Buffer.from( `\x1b%-12345X@PJL COMMENT *Start Job* \r
@PJL JOB NAME = "Sample Job #1" \r
@PJL SET COPIES = 3 \r
@PJL SET RET = OFF \r
@PJL ENTER LANGUAGE = PCL \r
\x1bE. . . . PCL job . . . .\x1bE
\x1b%-12345X@PJL \r
@PJL EOJ\r
\x1b%-12345X` );

		const segments = [];
		( await process( code, segments, null, false ) ).should.deepEqual( code );

		segments.splice( 0 );
		( await process( code, segments ) ).should.deepEqual( code );

		segments.should.have.length( 2 );
		const [ first, last ] = segments;

		first.commands.length.should.equal( 5 );
		last.commands.length.should.equal( 2 );

		first.commands.map( cmd => cmd.name ).should.deepEqual( [ "COMMENT", "JOB", "SET", "SET", "ENTER" ] );
		last.commands.map( cmd => cmd.name ).should.deepEqual( [ undefined, "EOJ" ] );
	} );

	it( "page #34 bottom works", async() => {
		const code = Buffer.from( `\x1b%-12345X@PJL \r
@PJL COMMENT ** Beginning PCL Job ** \r
@PJL ENTER LANGUAGE = PCL \r
@PJL SET LPARM : PCL SYMSET = DESKTOP \r
\x1bE . . . . PCL job . . . .\x1bE
\x1b%-12345X@PJL COMMENT End PCL \r
@PJL COMMENT Ready for PostScript Job \r
@PJL ENTER LANGUAGE = POSTSCRIPT \r
%!PS-ADOBE ... PostScript print job ... \x04\r
\x1b%-12345X` );

		const segments = [];
		( await process( code, segments, null, false ) ).should.deepEqual( code );

		segments.splice( 0 );
		( await process( code, segments ) ).should.deepEqual( code );

		segments.should.have.length( 3 );
		const [ first, second, third ] = segments;

		first.commands.length.should.equal( 3 );
		second.commands.length.should.equal( 3 );
		third.commands.length.should.equal( 0 );

		first.commands.map( cmd => cmd.name ).should.deepEqual( [ undefined, "COMMENT", "ENTER" ] );
		second.commands.map( cmd => cmd.name ).should.deepEqual( [ "COMMENT", "COMMENT", "ENTER" ] );
	} );

	it( "at typical document as generated by printer drivers of some commercial vendors works", async() => {
		// NOTE: this example is violating PJL specs as final UEL is missing
		const code = Buffer.from( `\x1b%-12345X@PJL JOB\r
@PJL SET AUTHENTICATIONENCRYPTSETTING = 2\r
@PJL SET AUTHENTICATIONUSERNAME = "123456"\r
@PJL SET AUTHENTICATIONPASSWORDCHARSET = 2\r
@PJL SET AUTHENTICATIONPASSWORD = "6tMIpklpE8PfvkqWhJ/OQw=="\r
@PJL SET AUTHENTICATIONENCRYPTMETHOD = 10\r
@PJL SET DUPLEXMODE = OFF\r
@PJL SET AUTHENTICATIONUSERNAMECHARSET = 2\r
@PJL ENTER LANGUAGE = POSTSCRIPT\r
%!PS-Adobe-3.0\r
%%Title: Microsoft Word - foo.doc\r
%%Creator: PScript5.dll Version 5.2.2\r
%%CreationDate: 1/28/2022 9:9:22\r
%%For: JohnDoe\r
%%BoundingBox: (atend)\r
%%Pages: (atend)\r
%%Orientation: Portrait\r
%%PageOrder: Special\r
%%DocumentNeededResources: (atend)\r
%%DocumentSuppliedResources: (atend)\r
%%DocumentData: Clean7Bit\r
%%TargetDevice: (VENDOR MODEL) (3018.102) 2\r
%%LanguageLevel: 3\r
%%EndComments\r
\r
%%BeginDefaults\r
%%PageBoundingBox: 12 12 571 818\r
%%ViewingOrientation: 1 0 0 1\r
%%EndDefaults\r
%%EOF\r
\x1b%-12345X@PJL EOJ\r
` );

		const segments = [];
		await process( code, segments ).should.be.rejectedWith( /incomplete PJL ticket/ );
		await process( code, segments, null, true ).should.be.rejectedWith( /incomplete PJL ticket/ );
		await process( code, segments, null, null ).should.be.rejectedWith( /incomplete PJL ticket/ );
		await process( code, segments, null, undefined ).should.be.rejectedWith( /incomplete PJL ticket/ );

		segments.splice( 0 );
		const processed = await process( code, segments, null, false );

		// stream gets fixed by parser by appending initially missing final UEL
		processed.should.deepEqual( Buffer.concat( [ code, Buffer.from( "\x1b%-12345X" ) ] ) );

		segments.should.have.length( 2 );
		const [ first, second ] = segments;

		first.commands.length.should.equal( 9 );
		second.commands.length.should.equal( 1 );

		first.commands.map( cmd => cmd.name ).should.deepEqual( [ "JOB", "SET", "SET", "SET", "SET", "SET", "SET", "SET", "ENTER" ] );
		second.commands.map( cmd => cmd.name ).should.deepEqual( ["EOJ"] );
	} );
} );

/**
 * Processes provided buffer looking for containing PJL segments to be collected
 * in provided collector.
 *
 * @param {Buffer} buffer buffer to parse for contained PJL
 * @param {Segment[]} segments reported segments extracted from buffer
 * @param {function} handler callback to invoke explicitly for handling discovered PJL segments
 * @param {boolean} strict if true, PJL parser fails on documents violating PJL specifications
 * @returns {Promise<Buffer>} promises all passing data
 */
async function process( buffer, segments, handler = null, strict = true ) {
	const source = new BufferStream.Reader( buffer );
	const filter = new Stream( { strict } );
	const sink = new BufferStream.Writer();

	filter.on( "pjl-segment", handler || ( segment => {
		segments.push( segment );
	} ) );

	source.pipe( filter ).pipe( sink );
	source.once( "error", cause => filter.destroy( cause ) );
	filter.once( "error", cause => sink.destroy( cause ) );

	return await sink.asPromise;
}
