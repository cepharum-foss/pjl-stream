import { describe, it } from "mocha";
import "should";

import { PJLParser } from "../lib/parsers/pjl.mjs";
import { Segment } from "../lib/segment.mjs";
import { CommandWithOptions, CommandWithWords, NopCommand } from "../lib/commands/index.mjs";
import { StartParser } from "../lib/parsers/start.mjs";
import { EndParser } from "../lib/parsers/end.mjs";

describe( "PJLParser", () => {
	it( "is an exposed class", () => {
		PJLParser.should.be.Function();
	} );

	describe( "exposes static method fromBuffer() which", () => {
		it( "is a function", () => {
			PJLParser.fromBuffer.should.be.a.Function();
		} );

		it( "expects one argument", () => {
			PJLParser.fromBuffer.should.have.length( 1 );
			( () => PJLParser.fromBuffer() ).should.throw();
		} );

		it( "expects Buffer to parse", () => {
			( () => PJLParser.fromBuffer( undefined ) ).should.throw();
			( () => PJLParser.fromBuffer( null ) ).should.throw();
			( () => PJLParser.fromBuffer( false ) ).should.throw();
			( () => PJLParser.fromBuffer( true ) ).should.throw();
			( () => PJLParser.fromBuffer( 0 ) ).should.throw();
			( () => PJLParser.fromBuffer( 3324 ) ).should.throw();
			( () => PJLParser.fromBuffer( -32.435 ) ).should.throw();
			( () => PJLParser.fromBuffer( "" ) ).should.throw();
			( () => PJLParser.fromBuffer( "@PJL\r\n" ) ).should.throw();
			( () => PJLParser.fromBuffer( [] ) ).should.throw();
			( () => PJLParser.fromBuffer( [Buffer.from( "@PJL\r\n" )] ) ).should.throw();
			( () => PJLParser.fromBuffer( {} ) ).should.throw();
			( () => PJLParser.fromBuffer( { toString: () => "@PJL\r\n" } ) ).should.throw();
			( () => PJLParser.fromBuffer( { lines: [Buffer.from( "@PJL\r\n" )] } ) ).should.throw();
			( () => PJLParser.fromBuffer( { code: Buffer.from( "@PJL\r\n" ) } ) ).should.throw();
			( () => PJLParser.fromBuffer( { code: Buffer.from( "@PJL\r\n" ) } ) ).should.throw();
			( () => PJLParser.fromBuffer( () => "@PJL\r\n" ) ).should.throw();
			( () => PJLParser.fromBuffer( () => Buffer.from( "@PJL\r\n" ) ) ).should.throw();

			( () => PJLParser.fromBuffer( Buffer.from( "@PJL\r\n" ) ) ).should.not.throw();
		} );

		it( "delivers instance of Segment containing all parsed commands", () => {
			const doc = PJLParser.fromBuffer( Buffer.from( "@PJL\r\n" ) );

			doc.should.be.instanceOf( Segment );
			doc.commands.should.have.length( 1 );
			doc.commands[0].should.be.instanceOf( NopCommand );

			const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH=y NUP=4 WATERMARK="helLO world!"\r\n`;
			const doc2 = PJLParser.fromBuffer( Buffer.from( code ) );

			doc2.should.be.instanceOf( Segment );
			doc2.commands.should.have.length( 3 );
			doc2.commands[0].should.be.instanceOf( NopCommand );

			doc2.commands[1].should.be.instanceOf( CommandWithWords );
			doc2.commands[1].name.should.equal( "COMMENT" );
			doc2.commands[1].words.should.deepEqual( [ "foo", "bar" ] );

			doc2.commands[2].should.be.instanceOf( CommandWithOptions );
			doc2.commands[2].name.should.equal( "SET" );
			doc2.commands[2].modifier.name.should.equal( "LANGUAGE" );
			doc2.commands[2].modifier.value.should.equal( "PCL" );
			doc2.commands[2].options.should.have.length( 3 );
			doc2.commands[2].options[0].name.should.equal( "PUNCH" );
			doc2.commands[2].options[0].value.should.equal( "y" );
			doc2.commands[2].options[1].name.should.equal( "NUP" );
			doc2.commands[2].options[1].value.should.equal( 4 );
			doc2.commands[2].options[2].name.should.equal( "WATERMARK" );
			doc2.commands[2].options[2].value.should.equal( "helLO world!" );
		} );

		it( "provides segment rendering as same sequence of characters as provided unless changing segment", () => {
			const code = `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y  NUP=4 \tWATERMARK\t=\t"helLO world!"\r\n`;
			const doc = PJLParser.fromBuffer( Buffer.from( code ) );

			String( doc ).should.equal( code );

			doc.commands[2].options[2].value = 2;

			String( doc ).should.not.equal( code );
		} );

		it( "rejects segments with lines not ending with line break", () => {
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y\r` ) ) ).should.throw();
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y\r\n` ) ) ).should.not.throw();
		} );

		it( "rejects segments with lines not starting with @PJL immediately", () => {
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n @PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y\r\n` ) ) ).should.throw();
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y\r\n` ) ) ).should.not.throw();
		} );

		it( "rejects segments with format #4 commands using invalid modifier name", () => {
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET 2LANGUAGE:PCL PUNCH = y\r\n` ) ) ).should.throw();
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y\r\n` ) ) ).should.not.throw();
		} );

		it( "rejects segments with format #4 commands using invalid modifier value", () => {
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:0PCL PUNCH = y\r\n` ) ) ).should.throw();
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y\r\n` ) ) ).should.not.throw();
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:"0PCL" PUNCH = y\r\n` ) ) ).should.not.throw();
		} );

		it( "rejects segments with format #4 commands using invalid option name", () => {
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL 0PUNCH = y\r\n` ) ) ).should.throw();
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL PUNCH = y\r\n` ) ) ).should.not.throw();
		} );

		it( "rejects segments with format #4 commands using invalid option value", () => {
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL SPEED = .5\r\n` ) ) ).should.throw();
			( () => PJLParser.fromBuffer( Buffer.from( `@PJL\r\n@PJL COMMENT foo  bar \r\n@PJL SET LANGUAGE:PCL SPEED = 0.5\r\n` ) ) ).should.not.throw();
		} );
	} );

	describe( "exposes static method parseValue() which", () => {
		it( "is a function", () => {
			PJLParser.parseValue.should.be.a.Function();
		} );

		it( "expects one argument", () => {
			PJLParser.parseValue.should.have.length( 1 );
			( () => PJLParser.parseValue() ).should.throw();
		} );

		it( "expects string argument to parse value from", () => {
			( () => PJLParser.parseValue( undefined ) ).should.throw();
			( () => PJLParser.parseValue( null ) ).should.throw();
			( () => PJLParser.parseValue( 0 ) ).should.throw();
			( () => PJLParser.parseValue( 234 ) ).should.throw();
			( () => PJLParser.parseValue( -76.392 ) ).should.throw();
			( () => PJLParser.parseValue( [] ) ).should.throw();
			( () => PJLParser.parseValue( ["foo"] ) ).should.throw();
			( () => PJLParser.parseValue( { input: "foo" } ) ).should.throw();
			( () => PJLParser.parseValue( { toString: () => "foo" } ) ).should.throw();
			( () => PJLParser.parseValue( () => "foo" ) ).should.throw();

			( () => PJLParser.parseValue( "foo" ) ).should.not.throw();
		} );

		it( "delivers object containing parsed value and rest of input following extracted value on success", () => {
			const result = PJLParser.parseValue( "3.5 bar" );

			result.should.be.Object().which.has.size( 2 ).and.has.properties( "value", "tail" );
			result.value.should.equal( 3.5 );
			result.tail.should.equal( " bar" );
		} );

		it( "ignores leading whitespace on parsing", () => {
			const result = PJLParser.parseValue( " \t 3.5 bar" );

			result.should.be.Object().which.has.size( 2 ).and.has.properties( "value", "tail" );
			result.value.should.equal( 3.5 );
			result.tail.should.equal( " bar" );
		} );

		it( "fails on value preceded by unsupported non-whitespace code", () => {
			( () => PJLParser.parseValue( " ' 3.5 bar" ) ).should.throw();
		} );

		it( "fails on invalid code not representing any supported kind of literal value", () => {
			( () => PJLParser.parseValue( ".5 bar" ) ).should.throw();
			( () => PJLParser.parseValue( "0test" ) ).should.throw();
			( () => PJLParser.parseValue( '"0test' ) ).should.throw();
		} );
	} );
} );

describe( "StartParser looking for beginning of PJL ticket in a stream", () => {
	it( "can be created w/o arguments", () => {
		const p = new StartParser();

		p.strict.should.be.true();
	} );

	it( "can be created w/ empty set of options", () => {
		const p = new StartParser( {} );

		p.strict.should.be.true();
	} );

	it( "ignores nullish option `strict`", () => {
		let p = new StartParser( { strict: undefined } );

		p.strict.should.be.true();

		p = new StartParser( { strict: null } );

		p.strict.should.be.true();
	} );

	it( "represents boolean option `strict` provided on construction", () => {
		let p = new StartParser( { strict: true } );

		p.strict.should.be.true();

		p = new StartParser( { strict: "foo" } );

		p.strict.should.be.true();

		p = new StartParser( { strict: [] } );

		p.strict.should.be.true();

		p = new StartParser( { strict: "" } );

		p.strict.should.be.false();

		p = new StartParser( { strict: false } );

		p.strict.should.be.false();
	} );
} );

describe( "EndParser looking for end of PJL ticket in a stream", () => {
	it( "can be created w/o arguments", () => {
		const p = new EndParser();

		p.strict.should.be.true();
	} );

	it( "can be created w/ empty set of options", () => {
		const p = new EndParser( {} );

		p.strict.should.be.true();
	} );

	it( "ignores nullish option `strict`", () => {
		let p = new EndParser( { strict: undefined } );

		p.strict.should.be.true();

		p = new EndParser( { strict: null } );

		p.strict.should.be.true();
	} );

	it( "represents boolean option `strict` provided on construction", () => {
		let p = new EndParser( { strict: true } );

		p.strict.should.be.true();

		p = new EndParser( { strict: "foo" } );

		p.strict.should.be.true();

		p = new EndParser( { strict: [] } );

		p.strict.should.be.true();

		p = new EndParser( { strict: "" } );

		p.strict.should.be.false();

		p = new EndParser( { strict: false } );

		p.strict.should.be.false();
	} );
} );
