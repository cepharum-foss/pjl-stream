import { describe, it } from "mocha";
import "should";
import { Value, Option } from "../lib/option.mjs";

describe( "Option", () => {
	it( "is exposed", () => {
		( Option != null ).should.be.true();
	} );

	it( "requires valid name on construction", () => {
		// NOK: lacking any argument
		( () => new Option() ).should.throw();

		// NOK: nullish name
		( () => new Option( null ) ).should.throw();
		( () => new Option( undefined ) ).should.throw();

		// NOK: stringifying name results in empty string
		( () => new Option( "" ) ).should.throw();
		( () => new Option( " \t " ) ).should.throw();
		( () => new Option( [] ) ).should.throw();
		( () => new Option( { toString: () => "  " } ) ).should.throw();

		// NOK: stringifying name results in string containing inner whitespace
		( () => new Option( {} ) ).should.throw();
		( () => new Option( { toString: () => " foo \tbar " } ) ).should.throw();
		( () => new Option( () => "foo" ) ).should.throw();

		// OK: stringifying name results in non-empty string without inner whitespace
		( () => new Option( false ) ).should.not.throw();
		( () => new Option( true ) ).should.not.throw();
		( () => new Option( "foo" ) ).should.not.throw();
		( () => new Option( " foo\n \r" ) ).should.not.throw();
		( () => new Option( ["foo"] ) ).should.not.throw();
		( () => new Option( { toString: () => " foo \t" } ) ).should.not.throw();
	} );

	it( "rejects name changed to invalid name after construction", () => {
		const option = new Option( "foo" );

		// NOK: nullish name
		( () => { option.name = null; } ).should.throw();
		( () => { option.name = undefined; } ).should.throw();

		// NOK: stringifying name results in empty string
		( () => { option.name = ""; } ).should.throw();
		( () => { option.name = " \t "; } ).should.throw();
		( () => { option.name = []; } ).should.throw();
		( () => { option.name = { toString: () => "  " }; } ).should.throw();

		// NOK: stringifying name results in string containing inner whitespace
		( () => { option.name = {}; } ).should.throw();
		( () => { option.name = { toString: () => " foo \tbar " }; } ).should.throw();
		( () => { option.name = () => "foo"; } ).should.throw();

		// OK: stringifying name results in non-empty string without inner whitespace
		( () => { option.name = false; } ).should.not.throw();
		( () => { option.name = true; } ).should.not.throw();
		( () => { option.name = "foo"; } ).should.not.throw();
		( () => { option.name = " foo\n \r"; } ).should.not.throw();
		( () => { option.name = ["foo"]; } ).should.not.throw();
		( () => { option.name = { toString: () => " foo \t" }; } ).should.not.throw();
	} );

	it( "accepts option's value as stringifiable data in second argument on construction", () => {
		// OK: lacking any value defaulting to nullish value
		( () => new Option( "foo" ) ).should.not.throw();

		// OK: nullish value
		( () => new Option( "foo", null ) ).should.not.throw();
		( () => new Option( "foo", undefined ) ).should.not.throw();

		// OK: stringifying name results in empty string
		( () => new Option( "foo", "" ) ).should.not.throw();
		( () => new Option( "foo", " \t " ) ).should.not.throw();
		( () => new Option( "foo", [] ) ).should.not.throw();
		( () => new Option( "foo", { toString: () => "  " } ) ).should.not.throw();

		// OK: stringifying name results in string containing inner whitespace
		( () => new Option( "foo", {} ) ).should.not.throw();
		( () => new Option( "foo", { toString: () => " foo \tbar " } ) ).should.not.throw();
		( () => new Option( "foo", () => 'foo' ) ).should.not.throw(); // eslint-disable-line quotes

		// NOK: stringifying name results in string containing vertical whitespace
		( () => new Option( "foo", { toString: () => " foo \nbar " } ) ).should.throw();
		( () => new Option( "foo", { toString: () => "\r foo \tbar " } ) ).should.throw();
		( () => new Option( "foo", { toString: () => " foo \tbar \n" } ) ).should.throw();

		// OK: stringifying name results in non-empty string without inner whitespace
		( () => new Option( "foo", false ) ).should.not.throw();
		( () => new Option( "foo", true ) ).should.not.throw();
		( () => new Option( "foo", "foo" ) ).should.not.throw();
		( () => new Option( "foo", ["foo"] ) ).should.not.throw();
		( () => new Option( "foo", { toString: () => " foo \t" } ) ).should.not.throw();
	} );

	it( "accepts option's value as instance of Value in second argument on construction", () => {
		( () => new Option( "foo", new Value() ) ).should.not.throw();
		( () => new Option( "foo", new Value( null ) ) ).should.not.throw();
		( () => new Option( "foo", new Value( "1.234" ) ) ).should.not.throw();
	} );

	it( "accepts value as stringifiable data after construction", () => {
		// OK: nullish value
		( () => { new Option( "foo" ).value = null; } ).should.not.throw();
		( () => { new Option( "foo" ).value = undefined; } ).should.not.throw();

		// OK: stringifying name results in empty string
		( () => { new Option( "foo" ).value = ""; } ).should.not.throw();
		( () => { new Option( "foo" ).value = " \t "; } ).should.not.throw();
		( () => { new Option( "foo" ).value = []; } ).should.not.throw();
		( () => { new Option( "foo" ).value = { toString: () => "  " }; } ).should.not.throw();

		// OK: stringifying name results in string containing inner whitespace
		( () => { new Option( "foo" ).value = {}; } ).should.not.throw();
		( () => { new Option( "foo" ).value = { toString: () => " foo \tbar " }; } ).should.not.throw();
		( () => { new Option( "foo" ).value = () => 'foo'; } ).should.not.throw(); // eslint-disable-line quotes

		// NOK: stringifying name results in string containing vertical whitespace
		( () => { new Option( "foo" ).value = { toString: () => " foo \nbar " }; } ).should.throw();
		( () => { new Option( "foo" ).value = { toString: () => "\r foo \tbar " }; } ).should.throw();
		( () => { new Option( "foo" ).value = { toString: () => " foo \tbar \n" }; } ).should.throw();

		// OK: stringifying name results in non-empty string without inner whitespace
		( () => { new Option( "foo" ).value = false; } ).should.not.throw();
		( () => { new Option( "foo" ).value = true; } ).should.not.throw();
		( () => { new Option( "foo" ).value = "foo"; } ).should.not.throw();
		( () => { new Option( "foo" ).value = ["foo"]; } ).should.not.throw();
		( () => { new Option( "foo" ).value = { toString: () => " foo \t" }; } ).should.not.throw();
	} );

	it( "accepts value as instance of Value after construction", () => {
		( () => { new Option( "foo" ).value = new Value(); } ).should.not.throw();
		( () => { new Option( "foo" ).value = new Value( null ); } ).should.not.throw();
		( () => { new Option( "foo" ).value = new Value( "1.234" ); } ).should.not.throw();
	} );

	it( "exposes its name", () => {
		new Option( "foo" ).name.should.equal( "foo" );
	} );

	it( "exposes its normalized value", () => {
		( new Option( "foo" ).value == null ).should.be.true();
		( new Option( "foo", undefined ).value == null ).should.be.true();
		new Option( "foo", "bar" ).value.should.equal( "bar" );
		new Option( "foo", "1.234" ).value.should.equal( 1.234 );
	} );

	it( "indicates no change after construction", () => {
		const option1 = new Option( "foo" );
		option1.changed.should.be.Boolean().which.is.false();

		const option2 = new Option( "foo", "test" );
		option2.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates no change after assigning same name as current", () => {
		const option = new Option( "foo" );
		option.name = "foo";
		option.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates no change after assigning same name as current wrapped in additional whitespace", () => {
		const option = new Option( "foo" );
		option.name = " \t foo \t ";
		option.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates change after assigning different name", () => {
		const option = new Option( "foo" );
		option.name = "fool";
		option.changed.should.be.Boolean().which.is.true();
	} );

	it( "indicates no change after assigning same value as current", () => {
		const option = new Option( "foo", "bar" );
		option.value = "bar";
		option.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates no change after assigning value stringified to same value as current", () => {
		const option = new Option( "foo", "bar" );
		option.value = { toString: () => "bar" };
		option.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates change after assigning different value of same type", () => {
		const option = new Option( "foo", "bar" );
		option.value = "bard";
		option.changed.should.be.Boolean().which.is.true();
	} );

	it( "indicates change after assigning different value of different type", () => {
		const option = new Option( "foo", "bar" );
		option.value = 1.234;
		option.changed.should.be.Boolean().which.is.true();
	} );

	describe( "exposes method render() for converting option to string which", () => {
		it( "is a function", () => {
			const option = new Option( "foo" );

			option.render.should.be.Function().which.has.length( 0 );
		} );

		it( "returns a string", () => {
			const option = new Option( "foo" );

			option.render().should.be.String();
		} );

		it( "returns just the name if value is nullish", () => {
			const option = new Option( "foo" );

			option.render().should.equal( "foo" );
		} );

		it( "returns just the name and value as assignment if value is non-nullish", () => {
			const option = new Option( "foo", "bar" );

			option.render().should.equal( "foo=bar" );
		} );

		it( "uses optionally provided separator instead of assignment operator", () => {
			const option = new Option( "foo", "bar" );

			option.render( ">>>" ).should.equal( "foo>>>bar" );
		} );

		it( "ignores optionally provided separator when value is nullish", () => {
			const option = new Option( "foo" );

			option.render( ">>>" ).should.equal( "foo" );
		} );

		it( "automatically wraps string values in double quotes", () => {
			const option = new Option( "foo", "hello world!" );

			option.render().should.equal( 'foo="hello world!"' );
			option.render( ":" ).should.equal( 'foo:"hello world!"' );
		} );
	} );

	it( "implicitly renders option when cast to string", () => {
		String( new Option( "foo" ) ).should.equal( "foo" );
		`>> ${new Option( "foo", "bar" )} <<`.should.equal( ">> foo=bar <<" );
	} );
} );

describe( "Value", () => {
	it( "is exposed", () => {
		( Value != null ).should.be.true();
	} );

	it( "can be constructed without initial value", () => {
		( () => new Value() ).should.not.throw();
		( new Value().value === null ).should.be.true();
	} );

	it( "can be constructed with nullish value", () => {
		( () => new Value( null ) ).should.not.throw();
		( new Value( null ).value === null ).should.be.true();

		( () => new Value( undefined ) ).should.not.throw();
		( new Value( undefined ).value === null ).should.be.true();
	} );

	it( "can be constructed with value stringified as empty string", () => {
		( () => new Value( "" ) ).should.not.throw();
		new Value( "" ).value.should.equal( "" );

		( () => new Value( [] ) ).should.not.throw();
		new Value( [] ).value.should.equal( "" );

		( () => new Value( { toString: () => "" } ) ).should.not.throw();
		new Value( { toString: () => "" } ).value.should.equal( "" );
	} );

	it( "can be constructed with value stringified as whitespace-only string", () => {
		( () => new Value( " \t " ) ).should.not.throw();
		new Value( " \t " ).value.should.equal( " \t " );

		( () => new Value( ["\t"] ) ).should.not.throw();
		new Value( ["\t"] ).value.should.equal( "\t" );

		( () => new Value( { toString: () => "  " } ) ).should.not.throw();
		new Value( { toString: () => "  " } ).value.should.equal( "  " );
	} );

	it( "rejects to be constructed with value stringified as string containing vertical whitespace", () => {
		( () => new Value( " \r " ) ).should.throw();
		( () => new Value( ["\n"] ) ).should.throw();
		( () => new Value( { toString: () => " \r\n " } ) ).should.throw();
		( () => new Value( "foo\r" ) ).should.throw();
		( () => new Value( "\nfoo" ) ).should.throw();
		( () => new Value( "f\roo" ) ).should.throw();
	} );

	it( "rejects to be constructed with value stringified as string containing non-whitespace control characters", () => {
		for ( let code = 0; code < 32; code++ ) {
			const char = String.fromCodePoint( code );

			if ( !/\s/.test( char ) ) {
				( () => new Value( char ) ).should.throw();
				( () => new Value( `${char} with valid suffix` ) ).should.throw();
				( () => new Value( `with valid prefix ${char}` ) ).should.throw();
				( () => new Value( `anywhere in between ${char} regular text` ) ).should.throw();
			}
		}
	} );

	it( "rejects to be constructed with value stringified as string containing double quotes", () => {
		( () => new Value( '"' ) ).should.throw();
		( () => new Value( `" with valid suffix` ) ).should.throw();
		( () => new Value( `with valid prefix "` ) ).should.throw();
		( () => new Value( `anywhere in "between " regular text` ) ).should.throw();
	} );

	it( "can be constructed with value stringifying as mix of printable characters and whitespace", () => {
		( () => new Value( {} ) ).should.not.throw();
		new Value( {} ).value.should.equal( "[object Object]" );

		( () => new Value( { toString: () => " foo \tbar " } ) ).should.not.throw();
		new Value( { toString: () => " foo \tbar " } ).value.should.equal( " foo \tbar " );

		( () => new Value( () => 'foo' ) ).should.not.throw(); // eslint-disable-line quotes
		new Value( () => 'foo' ).value.should.equal( "() => 'foo'" ); // eslint-disable-line quotes

		( () => new Value( false ) ).should.not.throw();
		new Value( false ).value.should.equal( "false" );

		( () => new Value( true ) ).should.not.throw();
		new Value( true ).value.should.equal( "true" );

		( () => new Value( "foo" ) ).should.not.throw();
		new Value( "foo" ).value.should.equal( "foo" );

		( () => new Value( " foo\t \t" ) ).should.not.throw();
		new Value( " foo\t \t" ).value.should.equal( " foo\t \t" );

		( () => new Value( ["foo"] ) ).should.not.throw();
		new Value( ["foo"] ).value.should.equal( "foo" );

		( () => new Value( { toString: () => " foo \t" } ) ).should.not.throw();
		new Value( { toString: () => " foo \t" } ).value.should.equal( " foo \t" );
	} );

	it( "can be constructed with value stringifying as numeric", () => {
		( () => new Value( 0 ) ).should.not.throw();
		new Value( 0 ).value.should.equal( 0 );

		( () => new Value( { toString: () => "3.4" } ) ).should.not.throw();
		new Value( { toString: () => "3.4" } ).value.should.equal( 3.4 );

		( () => new Value( "+200." ) ).should.not.throw();
		new Value( "+200." ).value.should.equal( 200 );

		( () => new Value( ["  -0.234 "] ) ).should.not.throw();
		new Value( ["  -0.234 "] ).value.should.equal( -0.234 );

		( () => new Value( 5e2 ) ).should.not.throw();
		new Value( 5e2 ).value.should.equal( 500 );
	} );

	it( "can be constructed with value stringifying as unsupported numeric", () => {
		( () => new Value( "NaN" ) ).should.not.throw();
		new Value( "NaN" ).value.should.equal( "NaN" );

		( () => new Value( "-Infinity" ) ).should.not.throw();
		new Value( "-Infinity" ).value.should.equal( "-Infinity" );

		( () => new Value( "+Infinity" ) ).should.not.throw();
		new Value( "+Infinity" ).value.should.equal( "+Infinity" );

		( () => new Value( NaN ) ).should.not.throw();
		new Value( NaN ).value.should.equal( "NaN" );

		( () => new Value( -Infinity ) ).should.not.throw();
		new Value( -Infinity ).value.should.equal( "-Infinity" );

		( () => new Value( Infinity ) ).should.not.throw();
		new Value( Infinity ).value.should.equal( "Infinity" );

		( () => new Value( "5e2" ) ).should.not.throw();
		new Value( "5e2" ).value.should.equal( "5e2" );
	} );

	it( "can be constructed with value taken from provided instance of Value", () => {
		new Value( new Value( "foo" ) ).value.should.equal( "foo" );
		new Value( new Value( 1.5 ) ).value.should.equal( 1.5 );
		new Value( new Value( { toString: () => " foo \tbar " } ) ).value.should.equal( " foo \tbar " );
		new Value( new Value( () => 'foo' ) ).value.should.equal( "() => 'foo'" ); // eslint-disable-line quotes
		new Value( new Value( false ) ).value.should.equal( "false" );
		new Value( new Value( true ) ).value.should.equal( "true" );
		new Value( new Value( "foo" ) ).value.should.equal( "foo" );
		new Value( new Value( " foo\t \t" ) ).value.should.equal( " foo\t \t" );
		new Value( new Value( ["foo"] ) ).value.should.equal( "foo" );
		new Value( new Value( { toString: () => " foo \t" } ) ).value.should.equal( " foo \t" );
	} );

	it( "accepts nullish value after construction", () => {
		const v1 = new Value();
		v1.value = null;
		( v1.value === null ).should.be.true();

		const v2 = new Value();
		v2.value = undefined;
		( v2.value === null ).should.be.true();
	} );

	it( "accepts value stringified as empty string after construction", () => {
		const v1 = new Value();
		v1.value = "";
		v1.value.should.equal( "" );

		const v2 = new Value();
		v2.value = [];
		v2.value.should.equal( "" );

		const v3 = new Value();
		v3.value = { toString: () => "" };
		v3.value.should.equal( "" );
	} );

	it( "accepts value stringified as whitespace-only string after construction", () => {
		const v1 = new Value();
		v1.value = " \t ";
		v1.value.should.equal( " \t " );

		const v2 = new Value();
		v2.value = ["\t"];
		v2.value.should.equal( "\t" );

		const v3 = new Value();
		v3.value = { toString: () => "  " };
		v3.value.should.equal( "  " );
	} );

	it( "rejects value stringified as string containing vertical whitespace after construction", () => {
		const v = new Value();

		( () => { v.value = " \r "; } ).should.throw();
		( v.value === null ).should.be.true();

		( () => { v.value = ["\n"]; } ).should.throw();
		( v.value === null ).should.be.true();

		( () => { v.value = { toString: () => " \r\n " }; } ).should.throw();
		( v.value === null ).should.be.true();

		( () => { v.value = "foo\r"; } ).should.throw();
		( v.value === null ).should.be.true();

		( () => { v.value = "\nfoo"; } ).should.throw();
		( v.value === null ).should.be.true();

		( () => { v.value = "f\roo"; } ).should.throw();
		( v.value === null ).should.be.true();
	} );

	it( "rejects value stringified as string containing non-whitespace control characters after construction", () => {
		for ( let code = 0; code < 32; code++ ) {
			const char = String.fromCodePoint( code );

			if ( !/\s/.test( char ) ) {
				const v1 = new Value();
				( () => { v1.value = char; } ).should.throw();

				const v2 = new Value();
				( () => { v2.value = `${char} with valid suffix`; } ).should.throw();

				const v3 = new Value();
				( () => { v3.value = `with valid prefix ${char}`; } ).should.throw();

				const v4 = new Value();
				( () => { v4.value = `anywhere in between ${char} regular text`; } ).should.throw();
			}
		}
	} );

	it( "rejects value stringified as string containing double quotes after construction", () => {
		const v1 = new Value();
		( () => { v1.value = '"'; } ).should.throw();

		const v2 = new Value();
		( () => { v2.value = `" with valid suffix`; } ).should.throw();

		const v3 = new Value();
		( () => { v3.value = `with valid prefix "`; } ).should.throw();

		const v4 = new Value();
		( () => { v4.value = `anywhere in "between " regular text`; } ).should.throw();
	} );

	it( "accepts value stringifying as mix of printable characters and whitespace after construction", () => {
		const v1 = new Value();
		v1.value = {};
		v1.value.should.equal( "[object Object]" );

		const v2 = new Value();
		v2.value = { toString: () => " foo \tbar " };
		v2.value.should.equal( " foo \tbar " );

		const v3 = new Value();
		v3.value = () => 'foo'; // eslint-disable-line quotes
		v3.value.should.equal( "() => 'foo'" );

		const v4 = new Value();
		v4.value = false;
		v4.value.should.equal( "false" );

		const v5 = new Value();
		v5.value = true;
		v5.value.should.equal( "true" );

		const v6 = new Value();
		v6.value = "foo";
		v6.value.should.equal( "foo" );

		const v7 = new Value();
		v7.value = " foo\t \t";
		v7.value.should.equal( " foo\t \t" );

		const v8 = new Value();
		v8.value = ["foo"];
		v8.value.should.equal( "foo" );

		const v9 = new Value();
		v9.value = { toString: () => " foo \t" };
		v9.value.should.equal( " foo \t" );
	} );

	it( "accepts value stringifying as numeric after construction", () => {
		const v1 = new Value();
		v1.value = 0;
		v1.value.should.equal( 0 );

		const v2 = new Value();
		v2.value = { toString: () => "3.4" };
		v2.value.should.equal( 3.4 );

		const v3 = new Value();
		v3.value = "+200.";
		v3.value.should.equal( 200 );

		const v4 = new Value();
		v4.value = ["  -0.234 "];
		v4.value.should.equal( -0.234 );

		const v5 = new Value();
		v5.value = 5e2;
		v5.value.should.equal( 500 );
	} );

	it( "accepts value stringifying as unsupported numeric after construction", () => {
		const v1 = new Value();
		v1.value = "NaN";
		v1.value.should.equal( "NaN" );

		const v2 = new Value();
		v2.value = "-Infinity";
		v2.value.should.equal( "-Infinity" );

		const v3 = new Value();
		v3.value = "+Infinity";
		v3.value.should.equal( "+Infinity" );

		const v4 = new Value();
		v4.value = NaN;
		v4.value.should.equal( "NaN" );

		const v5 = new Value();
		v5.value = -Infinity;
		v5.value.should.equal( "-Infinity" );

		const v6 = new Value();
		v6.value = Infinity;
		v6.value.should.equal( "Infinity" );

		const v7 = new Value();
		v7.value = "5e2";
		v7.value.should.equal( "5e2" );

		const v8 = new Value();
		v8.value = 5e2;
		v8.value.should.equal( 500 );
	} );

	it( "accepts value taken from provided instance of Value after construction", () => {
		const v1 = new Value();
		v1.value = new Value( "foo" );
		v1.value.should.equal( "foo" );

		const v2 = new Value();
		v2.value = new Value( 1.5 );
		v2.value.should.equal( 1.5 );

		const v3 = new Value();
		v3.value = new Value( { toString: () => " foo \tbar " } );
		v3.value.should.equal( " foo \tbar " );

		const v4 = new Value();
		v4.value = new Value( () => 'foo' ); // eslint-disable-line quotes
		v4.value.should.equal( "() => 'foo'" );

		const v5 = new Value();
		v5.value = new Value( false );
		v5.value.should.equal( "false" );

		const v6 = new Value();
		v6.value = new Value( true );
		v6.value.should.equal( "true" );

		const v7 = new Value();
		v7.value = new Value( "foo" );
		v7.value.should.equal( "foo" );

		const v8 = new Value();
		v8.value = new Value( " foo\t \t" );
		v8.value.should.equal( " foo\t \t" );

		const v9 = new Value();
		v9.value = new Value( ["foo"] );
		v9.value.should.equal( "foo" );

		const v10 = new Value();
		v10.value = new Value( { toString: () => " foo \t" } );
		v10.value.should.equal( " foo \t" );
	} );

	it( "renders as string instantly suitable for use in a PJL segment", () => {
		const v1 = new Value( "" );
		String( v1 ).should.equal( '""' );

		const v2 = new Value( 123 );
		String( v2 ).should.equal( "123" );

		const v3 = new Value( "AKeyword123" );
		String( v3 ).should.equal( "AKeyword123" );

		const v4 = new Value( "A_Keyword_123" );
		String( v4 ).should.equal( '"A_Keyword_123"' );

		const v5 = new Value( "A Keyword 123" );
		String( v5 ).should.equal( '"A Keyword 123"' );
	} );

	it( "rejects to render as string when nullish", () => {
		const v1 = new Value();
		( () => String( v1 ) ).should.throw();

		const v2 = new Value( null );
		( () => String( v2 ) ).should.throw();

		const v3 = new Value( undefined );
		( () => String( v3 ) ).should.throw();
	} );

	it( "indicates no change after construction", () => {
		const v1 = new Value();
		v1.changed.should.be.Boolean().which.is.false();

		const v2 = new Value( "test" );
		v2.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates no change after assigning same value as current", () => {
		const v = new Value( "bar" );
		v.value = "bar";
		v.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates no change after assigning value stringified to same value as current", () => {
		const v = new Value( "bar" );
		v.value = { toString: () => "bar" };
		v.changed.should.be.Boolean().which.is.false();
	} );

	it( "indicates change after assigning different value of same type", () => {
		const v = new Value( "bar" );
		v.value = "bard";
		v.changed.should.be.Boolean().which.is.true();
	} );

	it( "indicates change after assigning different value of different type", () => {
		const v = new Value( "bar" );
		v.value = 1.234;
		v.changed.should.be.Boolean().which.is.true();
	} );
} );
