import * as Crypto from "crypto";
import { toBuffer } from "../lib/roman-9.mjs";
import { UEL } from "../lib/basics.mjs";

/**
 * Generates random binary data and embeds provided PJL segments at arbitrary
 * positions.
 *
 * @param {int} documentSize size of generated document in number of octets
 * @param {number[]} forcedOffsets provides offsets to use explicitly on embedding, otherwise will pick random offsets
 * @param {Segment|string} pjlSegments PJL segments to embed
 * @returns {Buffer} generated document with PJL segments embedded at arbitrary positions
 */
export function generateRandomDocumentWithEmbeddedPJL( documentSize, forcedOffsets, ...pjlSegments ) {
	const buffers = Array.from( pjlSegments, pjlSegment => toBuffer( String( pjlSegment ) ) );
	const minLength = buffers.reduce( ( sum, buffer ) => sum + buffer.length + UEL.length, 0 );

	if ( documentSize < minLength ) {
		throw new TypeError( `insufficient document size for properly embedding provided PJL segments` );
	}

	if ( documentSize < 2 * minLength ) {
		console.warn( `document size close to size of PJL segments to embed for testing -> creating test document might fail` );
	}

	const binary = Crypto.randomBytes( documentSize );
	const _forced = Array.isArray( forcedOffsets ) ? forcedOffsets : [];
	let offsets, repeat, attempts;

	for ( attempts = 0, repeat = 1; attempts < 1000 && repeat > 0; attempts++, repeat-- ) {
		offsets = [ ..._forced, ...Array.from( buffers.slice( _forced.length ), () => Crypto.randomInt( 0, documentSize ) ) ].sort();

		if ( offsets.some( ( offset, i ) => offset + buffers[i].length + UEL.length >= ( offsets[i + 1] || documentSize ) ) ) {
			repeat = 1;
		}
	}

	if ( repeat ) {
		throw new Error( "multiple attempts of embedding PJL segments failed - giving up" );
	}

	buffers.forEach( ( buffer, index ) => {
		binary.copy( UEL, offsets[index] );
		buffer.copy( binary, offsets[index] + UEL.length );
	} );

	return binary;
}
