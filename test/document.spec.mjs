import { describe, it } from "mocha";
import "should";

import { Segment } from "../lib/segment.mjs";
import { Option } from "../lib/option.mjs";
import { CommandWithOptions, CommandWithWords } from "../lib/commands/index.mjs";

describe( "Segment", () => {
	it( "is an exposed class", () => {
		Segment.should.be.Function();
	} );

	it( "can be constructed without any argument", () => {
		( () => new Segment() ).should.not.throw();
	} );

	it( "exposes list of segment's commands", () => {
		new Segment().commands.should.be.Array().which.is.empty();
	} );

	it( "accepts command pushed to list", () => {
		const segment = new Segment();

		segment.commands.push( new CommandWithOptions( "foo", "bar" ) );

		segment.commands.should.have.length( 1 );
	} );

	it( "rejects pushing anything but a command", () => {
		const segment = new Segment();

		( () => segment.commands.push( undefined ) ).should.throw();
		( () => segment.commands.push( null ) ).should.throw();
		( () => segment.commands.push( false ) ).should.throw();
		( () => segment.commands.push( true ) ).should.throw();
		( () => segment.commands.push( 0 ) ).should.throw();
		( () => segment.commands.push( 100 ) ).should.throw();
		( () => segment.commands.push( -200.34 ) ).should.throw();
		( () => segment.commands.push( "" ) ).should.throw();
		( () => segment.commands.push( "foo=bar" ) ).should.throw();
		( () => segment.commands.push( [] ) ).should.throw();
		( () => segment.commands.push( [new CommandWithOptions( "foo", "bar" )] ) ).should.throw();
		( () => segment.commands.push( {} ) ).should.throw();
		( () => segment.commands.push( { name: "bar", raw: "foo" } ) ).should.throw();
		( () => segment.commands.push( () => new CommandWithOptions( "foo", "bar" ) ) ).should.throw();

		segment.commands.should.be.empty();
	} );

	it( "accepts command unshifted to list", () => {
		const segment = new Segment();

		segment.commands.unshift( new CommandWithOptions( "foo", "bar" ) );

		segment.commands.should.have.length( 1 );
	} );

	it( "rejects unshifting anything but a command", () => {
		const segment = new Segment();

		( () => segment.commands.unshift( undefined ) ).should.throw();
		( () => segment.commands.unshift( null ) ).should.throw();
		( () => segment.commands.unshift( false ) ).should.throw();
		( () => segment.commands.unshift( true ) ).should.throw();
		( () => segment.commands.unshift( 0 ) ).should.throw();
		( () => segment.commands.unshift( 100 ) ).should.throw();
		( () => segment.commands.unshift( -200.34 ) ).should.throw();
		( () => segment.commands.unshift( "" ) ).should.throw();
		( () => segment.commands.unshift( "foo=bar" ) ).should.throw();
		( () => segment.commands.unshift( [] ) ).should.throw();
		( () => segment.commands.unshift( [new CommandWithOptions( "foo", "bar" )] ) ).should.throw();
		( () => segment.commands.unshift( {} ) ).should.throw();
		( () => segment.commands.unshift( { name: "bar", raw: "foo" } ) ).should.throw();
		( () => segment.commands.unshift( () => new CommandWithOptions( "foo", "bar" ) ) ).should.throw();

		segment.commands.should.be.empty();
	} );

	it( "accepts command of list being replaced", () => {
		const segment = new Segment();

		segment.commands.splice( 100, 0, new CommandWithOptions( "foo", "bar" ) );
		segment.commands.splice( 1, 100, new CommandWithOptions( "foo", "bar" ) );

		segment.commands.should.have.length( 2 );
	} );

	it( "rejects listed commands with anything but a command", () => {
		const segment = new Segment();

		( () => segment.commands.splice( 100, 0, undefined ) ).should.throw();
		( () => segment.commands.splice( 100, 0, null ) ).should.throw();
		( () => segment.commands.splice( 100, 0, false ) ).should.throw();
		( () => segment.commands.splice( 100, 0, true ) ).should.throw();
		( () => segment.commands.splice( 100, 0, 0 ) ).should.throw();
		( () => segment.commands.splice( 100, 0, 100 ) ).should.throw();
		( () => segment.commands.splice( 100, 0, -200.34 ) ).should.throw();
		( () => segment.commands.splice( 100, 0, "" ) ).should.throw();
		( () => segment.commands.splice( 100, 0, "foo=bar" ) ).should.throw();
		( () => segment.commands.splice( 100, 0, [] ) ).should.throw();
		( () => segment.commands.splice( 100, 0, [new CommandWithOptions( "foo", "bar" )] ) ).should.throw();
		( () => segment.commands.splice( 100, 0, {} ) ).should.throw();
		( () => segment.commands.splice( 100, 0, { name: "bar", raw: "foo" } ) ).should.throw();
		( () => segment.commands.splice( 100, 0, () => new CommandWithOptions( "foo", "bar" ) ) ).should.throw();

		segment.commands.should.be.empty();
	} );

	it( "accepts command written to list", () => {
		const segment = new Segment();

		segment.commands[0] = new CommandWithOptions( "foo", "bar" );
		segment.commands[1] = new CommandWithOptions( "foo", "bar" );

		segment.commands.should.have.length( 2 );
	} );

	it( "rejects command written to list causing the latter to have sparse items", () => {
		const segment = new Segment();

		( () => { segment.commands[1] = new CommandWithOptions( "foo", "bar" ); } ).should.throw();
		( () => { segment.commands[100] = new CommandWithOptions( "foo", "bar" ); } ).should.throw();

		segment.commands.should.be.empty();
	} );

	it( "rejects writing anything but a command to list", () => {
		const segment = new Segment();

		( () => { segment.commands[0] = undefined; } ).should.throw();
		( () => { segment.commands[0] = null; } ).should.throw();
		( () => { segment.commands[0] = false; } ).should.throw();
		( () => { segment.commands[0] = true; } ).should.throw();
		( () => { segment.commands[0] = 0; } ).should.throw();
		( () => { segment.commands[0] = 100; } ).should.throw();
		( () => { segment.commands[0] = -200.34; } ).should.throw();
		( () => { segment.commands[0] = ""; } ).should.throw();
		( () => { segment.commands[0] = "foo=bar"; } ).should.throw();
		( () => { segment.commands[0] = []; } ).should.throw();
		( () => { segment.commands[0] = [new CommandWithOptions( "foo", "bar" )]; } ).should.throw();
		( () => { segment.commands[0] = {}; } ).should.throw();
		( () => { segment.commands[0] = { name: "bar", raw: "foo" }; } ).should.throw();
		( () => { segment.commands[0] = () => new CommandWithOptions( "foo", "bar" ); } ).should.throw();

		segment.commands.should.be.empty();
	} );

	it( "renders as empty string by default", () => {
		String( new Segment() ).should.be.equal( "" );
		`>>> ${new Segment()} <<<`.should.be.equal( ">>>  <<<" );
	} );

	it( "renders as concatenation of listed commands' serialization", () => {
		const segment = new Segment();

		segment.commands.push( new CommandWithWords( "foo", "echo", ["test"] ) );
		segment.commands.push( new CommandWithOptions( "bar", "set", new Option( "language", "pcl" ), [ new Option( "nup", 4 ), new Option( "sides", 2 ) ] ) );

		String( segment ).should.be.equal( "foobar" );
		`>>> ${segment} <<<`.should.be.equal( ">>> foobar <<<" );

		segment.commands[0].words = "hello";

		String( segment ).should.be.equal( "@PJL ECHO hello\r\nbar" );
		`>>> ${segment} <<<`.should.be.equal( ">>> @PJL ECHO hello\r\nbar <<<" );

		segment.commands[1].modifier.value = "ps";

		String( segment ).should.be.equal( "@PJL ECHO hello\r\n@PJL SET language:ps nup=4 sides=2\r\n" );
		`>>> ${segment} <<<`.should.be.equal( ">>> @PJL ECHO hello\r\n@PJL SET language:ps nup=4 sides=2\r\n <<<" );
	} );
} );
