import { Segment } from "../segment.mjs";
import {
	CommandWithOptions,
	CommandWithWords,
	NopCommand
} from "../commands/index.mjs";
import { fromBuffer } from "../roman-9.mjs";
import { Option } from "../option.mjs";

/**
 * Matches numeric value according to PJL technical specs.
 *
 * @type {RegExp}
 */
export const NumericValuePattern = /^\s*([+-]?\d+(\.\d*)?)(?=[^.\da-z]|$)/i;

/**
 * Matches alphanumeric value according to PJL technical specs.
 *
 * @type {RegExp}
 */
export const AlphanumericValuePattern = /^\s*([a-z][a-z0-9]*)/i;

/**
 * Matches string value according to PJL technical specs.
 *
 * @type {RegExp}
 */
export const StringValuePattern = /^\s*"([^"\x00-\x08\x0a-\x1f]*)"/i; // eslint-disable-line no-control-regex

/**
 * Parses PJL segment from sequence of octets.
 */
export class PJLParser {
	/**
	 * Parses series of PJL commands as PJL segment from provided buffer.
	 *
	 * @param {Buffer} buffer serialized PJL segment
	 * @returns {Segment} parsed PJL segment
	 */
	static fromBuffer( buffer ) {
		const code = fromBuffer( buffer );
		const job = new Segment();
		const linePtn = /@PJL(?:[ \t]*|[ \t]+([a-zA-Z][a-zA-Z0-9]*)(?:[ \t]+(\S.*)|[ \t]*)?)?\r?\n/g;

		while ( linePtn.lastIndex < code.length ) {
			const cursor = linePtn.lastIndex;
			const line = linePtn.exec( code );

			if ( !line || line.index > cursor ) {
				throw new Error( `invalid code in PJL line starting at index #${cursor} reading: ${code.substring( cursor, 80 ).replace( /\r\n[\s\S]+/, "" )}` );
			}

			if ( line[1] == null ) {
				// format #2 PJL command -> NOP ...
				job.commands.push( new NopCommand( line[0] ) );
				continue;
			}

			const command = line[1].toUpperCase();
			let tail = line[2] == null ? "" : line[2].trim();

			switch ( command ) {
				case "COMMENT" :
				case "ECHO" : {
					// must be format #3 PJL command
					job.commands.push( new CommandWithWords( line[0], command, tail ) );
					break;
				}

				default : {
					// must be format #4 PJL command

					// extract optional modifier
					const modifierMatch = /^([a-zA-Z][a-zA-Z0-9]*)\s*:\s*(\S.*)$/.exec( tail );
					let modifier;

					if ( modifierMatch ) {
						const { value, tail: modifierTail } = this.parseValue( modifierMatch[2] );
						modifier = new Option( modifierMatch[1], value );
						tail = modifierTail.trim();
					}

					// extract list of options
					const options = [];

					while ( tail.length > 0 ) {
						const optionMatch = /^([a-zA-Z][a-zA-Z0-9]*)\s*(?:=\s*(\S.*))?$/.exec( tail );

						if ( !optionMatch ) {
							throw new Error( `invalid code "${tail}" on expecting option for PJL command ${command}: "${line}"` );
						}

						const { value, tail: optionTail } = this.parseValue( optionMatch[2] );
						options.push( new Option( optionMatch[1], value ) );
						tail = optionTail.trim();
					}

					job.commands.push( new CommandWithOptions( line[0], command, modifier, options ) );
				}
			}
		}

		return job;
	}

	/**
	 * Parses value at beginning of provided input string.
	 *
	 * @param {string} input string containing value to be parsed at its beginning
	 * @returns {{tail: string, value: (number|string)}} found value and tail of provided string following that value
	 */
	static parseValue( input ) {
		if ( typeof input !== "string" ) {
			throw new TypeError( `input must be string, got ${typeof input}` );
		}

		input = input.replace( /^\s+/, "" ); // eslint-disable-line no-param-reassign

		const alphaMatch = AlphanumericValuePattern.exec( input );
		if ( alphaMatch && alphaMatch.index === 0 ) {
			return {
				value: alphaMatch[1],
				tail: input.substring( alphaMatch[0].length ),
			};
		}

		const numericMatch = NumericValuePattern.exec( input );
		if ( numericMatch && numericMatch.index === 0 ) {
			return {
				value: parseFloat( numericMatch[1] ),
				tail: input.substring( numericMatch[0].length ),
			};
		}

		const stringMatch = StringValuePattern.exec( input );
		if ( stringMatch && stringMatch.index === 0 ) {
			return {
				value: stringMatch[1],
				tail: input.substring( stringMatch[0].length ),
			};
		}

		throw new Error( `invalid PJL value: ${input}` );
	}
}
