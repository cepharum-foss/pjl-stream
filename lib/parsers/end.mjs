import {
	CollectingRaceParser, PatternParser, SubstringParser
} from "parsing-stream/index.mjs";

import { StartParser } from "./start.mjs";
import { toBuffer } from "../roman-9.mjs";
import { PJLParser } from "./pjl.mjs";
import { UEL } from "../basics.mjs";

/**
 * Parsing-stream compatible pattern for matching end of PJL segment.
 *
 * @type {string}
 */
const enterLanguageCmdPattern = "@PJL[ \t]+[Ee][Nn][Tt][Ee][Rr][ \t]+[Ll][Aa][Nn][Gg][Uu][Aa][Gg][Ee][ \t]*=[ \t]*[^\r\n]+\r?\n";

/**
 * Parses stream for matching UEL sequence starting (another) PJL job.
 *
 * @type {EndParser}
 * @name EndParser
 *
 * @param {boolean} strict if true, PJL parser fails on documents violating PJL specifications
 */
export class EndParser extends CollectingRaceParser {
	/**
	 * @param {boolean} strict if true, PJL parser fails on documents violating PJL specifications
	 */
	constructor( { strict = true } = {} ) {
		super( [
			new PatternParser( enterLanguageCmdPattern ),
			new SubstringParser( UEL ),
		], { tee: false } );

		this.strict = Boolean( strict ?? true );
	}

	/** @inheritDoc */
	parse( context, buffer, atOffset, isLast ) {
		this.foundPjl = false;

		let result = super.parse( context, buffer, atOffset, isLast );

		if ( isLast && !this.foundPjl ) {
			const numPreviouslyMatchingOctets = this.previouslyMatchingOctetsCount;

			if ( buffer.length + numPreviouslyMatchingOctets > 0 || context.collectedSomething ) {
				let fail = true;

				if ( !this.strict ) {
					// is code following last UEL well-formed PJL code?
					const tail = Buffer.concat( [ ...this.previouslyMatchingChunks, buffer ] );

					PJLParser.fromBuffer( tail );

					// yes -> consider a final match
					result = this.signalFullMatch( context, buffer, atOffset, -numPreviouslyMatchingOctets, buffer.length );

					fail = false;
				}

				if ( fail ) {
					this.stream.destroy( new Error( "incomplete PJL ticket" ) );
				}
			} else {
				// StartParser found another UEL but there is no octet
				// succeeding it -> UEL at end of stream -> valid closure of PJL
				result = this.signalFullMatch( context, buffer, atOffset, 0, 0 );
			}
		}

		return result;
	}

	/** @inheritDoc */
	signalFullMatch( ...args ) {
		const result = super.signalFullMatch( ...args );

		this.foundPjl = true;

		return result;
	}

	/** @inheritDoc */
	async onMatch( context, buffers, atOffset, parser ) {
		this.switchCollected();

		const collected = await context.pjl.asPromise;

		const chunks = parser instanceof PatternParser ? [...buffers] : [];
		chunks.unshift( collected );

		const data = Buffer.concat( chunks );
		const pjl = PJLParser.fromBuffer( data );

		this.stream.emit( "pjl-segment", pjl );

		const { commands } = pjl;
		const { length } = commands;
		const lastCommand = length > 0 ? commands[length - 1] : { name: "ENTER" };

		const needsUel = lastCommand.name !== "ENTER";
		const code = String( pjl );

		const hadCode = data.toString().length > 0;
		const hasCode = code.toString().length > 0;

		buffers.splice( 0, buffers.length, !hadCode || hasCode ? toBuffer( UEL + code + ( needsUel ? UEL : "" ) ) : Buffer.from( [] ) );

		this.pjlProcessed = true;

		return new StartParser( { strict: this.strict } );
	}
}
