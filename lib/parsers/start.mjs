import { Transform } from "stream";

import { SubstringParser, BufferStream } from "parsing-stream/index.mjs";

import { EndParser } from "./end.mjs";
import { UEL } from "../basics.mjs";
import { toBuffer } from "../roman-9.mjs";


/**
 * Parses stream for next UEL sequence assumed to start (another) PJL job.
 *
 * @type {StartParser}
 * @name StartParser
 *
 * @param {boolean} strict if true, PJL parser fails on documents violating PJL specifications
 */
export class StartParser extends SubstringParser {
	/**
	 * @param {boolean} strict if true, PJL parser fails on documents violating PJL specifications
	 */
	constructor( { strict = true } = {} ) {
		// search for command format #1, which is UEL command
		super( toBuffer( UEL ) );

		this.strict = Boolean( strict ?? true );
	}

	/** @inheritDoc */
	onMatch( context, buffers ) {
		const end = new EndParser( { strict: this.strict } );

		context.collectedSomething = false; // eslint-disable-line no-param-reassign

		const monitorCollector = new Transform( {
			transform( chunk, encoding, callback ) {
				if ( chunk.length > 0 ) {
					context.collectedSomething = true; // eslint-disable-line no-param-reassign
				}

				this.push( chunk, encoding );
				callback();
			}
		} );

		context.pjl = end.collected // eslint-disable-line no-param-reassign
			.pipe( monitorCollector )
			.pipe( new BufferStream.Writer() );

		// don't put UEL in stream now ... will be written as part of end parser
		buffers.splice( 0 );

		return end;
	}
}
