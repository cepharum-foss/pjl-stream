import { toOctet } from "./roman-9.mjs";
import { AlphanumericValuePattern, NumericValuePattern } from "./parsers/pjl.mjs";

/**
 * Implements single option consisting of a name and an optional value.
 *
 * @property {string} name name of option
 * @property {Value} value current value of option
 * @property {boolean} changed true if option has changed since construction
 */
export class Option {
	/**
	 * @param {string} name name of option
	 * @param {(Value|*)?} value value of option, gets stringified
	 */
	constructor( name, value = null ) {
		let changed = false;

		name = this.normalizeName( name ); // eslint-disable-line no-param-reassign
		value = new Value( value instanceof Value ? value.value : value ); // eslint-disable-line no-param-reassign

		Object.defineProperties( this, {
			name: {
				get: () => name,
				set( newValue ) {
					const asString = this.normalizeName( newValue );

					if ( asString !== name ) {
						name = asString; // eslint-disable-line no-param-reassign
						changed = true;
					}
				},
			},

			value: {
				get: () => value.value,
				set: newValue => {
					value.value = newValue; // eslint-disable-line no-param-reassign
				},
			},

			_value: { value: value },

			changed: {
				get: () => changed || value.changed,
			},
		} );
	}

	/**
	 * Normalizes and validates name of command's option.
	 *
	 * @param {any} name new name of option to be normalized and validated, might be null to drop any option's name and value
	 * @returns {string} normalized and validated name of option
	 * @throws TypeError if validation fails
	 * @protected
	 */
	normalizeName( name ) {
		if ( name == null ) {
			throw new TypeError( "option name must not be nullish" );
		}

		const asString = String( name ).trim();

		if ( asString.length < 1 ) {
			throw new TypeError( "option name must not be empty" );
		}

		if ( /\s/.test( asString ) ) {
			throw new TypeError( "option name must not contain whitespace" );
		}

		return asString;
	}

	/**
	 * Renders current option as string using provided string for separating
	 * name from value.
	 *
	 * @param {string} separator string separating name from value in rendered string
	 * @returns {string} option rendered as string
	 */
	render( separator = "=" ) {
		if ( this.value != null ) {
			return `${this.name}${separator}${this._value}`;
		}

		return this.name;
	}

	/** @inheritDoc */
	toString() {
		return this.render( "=" );
	}
}

/**
 * Represents PJL value.
 *
 * @property {string|number|null} value provides wrapped value
 * @property {boolean} changed true if value has been changed since construction
 */
export class Value {
	/**
	 * @param {*?} value value to wrap, gets stringified
	 */
	constructor( value ) {
		let changed = false;

		value = this.normalize( value ); // eslint-disable-line no-param-reassign

		Object.defineProperties( this, {
			value: {
				get: () => value,
				set( newValue ) {
					const normalized = this.normalize( newValue );

					if ( normalized !== value ) {
						value = normalized; // eslint-disable-line no-param-reassign
						changed = true;
					}
				}
			},

			changed: { get: () => changed },
		} );
	}

	/**
	 * Normalizes and validates value of command's option.
	 *
	 * @param {any} value new value of option to be normalized and validated, must be nullish or some string with printable character
	 * @returns {string|number|null} normalized and validated value of option
	 * @throws TypeError if validation fails
	 * @protected
	 */
	normalize( value ) {
		if ( value == null ) {
			return null;
		}

		if ( value instanceof Value ) {
			return value.value;
		}

		const asString = String( value );

		let match = NumericValuePattern.exec( asString );
		if ( match && match[0].trim() === asString.trim() ) {
			return parseFloat( asString );
		}

		match = AlphanumericValuePattern.exec( asString );
		if ( match && match[0].trim() === asString.trim() && !/\s/.test( asString ) ) {
			return asString;
		}

		if ( ![].some.call( asString, char => {
			const code = toOctet.get( char );

			return isNaN( code ) || ( code < 32 && code !== 9 ) || code === 34;
		} ) ) {
			return asString;
		}

		throw new TypeError( `invalid value "${asString}"` );
	}

	/** @inheritDoc */
	toString() {
		const { value } = this;

		if ( value == null ) {
			throw new Error( "cannot render nullish value" );
		}

		if ( typeof value === "number" ) {
			return String( value );
		}

		const match = AlphanumericValuePattern.exec( value );
		if ( match && match[0] === value ) {
			return value;
		}

		return `"${value}"`;
	}
}
