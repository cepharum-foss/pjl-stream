import { Stream as ParsingStream } from "parsing-stream/index.mjs";
import { StartParser } from "./parsers/start.mjs";

/**
 * Parses passing data for PJL segments.
 *
 * @name Stream
 * @type {Stream}
 */
export class Stream extends ParsingStream {
	/**
	 * @param {boolean} strict if true, PJL parser fails on documents violating PJL specifications
	 */
	constructor( { strict = true } = {} ) {
		super( {
			initialParser: new StartParser( { strict: Boolean( strict ?? true ) } ),
		} );
	}
}
