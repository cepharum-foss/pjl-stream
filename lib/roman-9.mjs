/** Describes character with encoding identical to character's index in this table. */
const same = {};

/** Describes octet of encoding table that doesn't have a related character. */
const none = null;

/**
 * Implements HP's Roman-9 encoding table.
 *
 * @type {Array<(number|object)>}
 */
const table = [
	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, same,
	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, same,
	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, same,
	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, same,

	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, same,
	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, same,
	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, same,
	same, same, same, same, same, same, same, same, same, same, same, same, same, same, same, 0x2592,

	none, none, none, none, none, none, none, none, none, none, none, none, none, none, none, none,
	none, none, none, none, none, none, none, none, none, none, none, none, none, none, none, none,
	0xa0, 0xc0, 0xc2, 0xc8, 0xca, 0xcb, 0xce, 0xcf, 0xb4, 0x2cb, 0x2c6, 0xa8, 0x2dc, 0xd9, 0xdb, 0x20a4,
	0xaf, 0xdd, 0xfd, 0xb0, 0xc7, 0xe7, 0xd1, 0xf1, 0xa1, 0xbf, 0x20ac, 0xa3, 0xa5, 0xa7, 0x192, 0xa2,

	0xe2, 0xea, 0xf4, 0xfb, 0xe1, 0xe9, 0xf3, 0xfa, 0xe0, 0xe8, 0xf2, 0xf9, 0xe4, 0xeb, 0xf6, 0xfc,
	0xc5, 0xee, 0xd8, 0xc6, 0xe5, 0xed, 0xf8, 0xe6, 0xc4, 0xec, 0xd6, 0xdc, 0xc9, 0xef, 0xdf, 0xd4,
	0xc1, 0xc3, 0xe3, 0xd0, 0xf0, 0xcd, 0xcc, 0xd3, 0xd2, 0xd5, 0xf5, 0x160, 0x161, 0xda, 0x178, 0xff,
	0xde, 0xfe, 0xb7, 0xb5, 0xb6, 0xbe, 0xad, 0xbc, 0xbd, 0xaa, 0xba, 0xab, 0x25a0, 0xbb, 0xb1, none,
];

/**
 * Exposes derived map for converting an octet into UTF-8 encoded character
 * according to Roman-9 encoding.
 *
 * @type {Map<number, string>}
 */
export const fromOctet = new Map( Array.from( table, ( code, octet ) => {
	return [ octet, code === none ? String.fromCodePoint( 0xe000 + octet ) : String.fromCodePoint( code === same ? octet : code ) ];
} ) );

/**
 * Exposes derived map for converting a UTF-8 encoded character into octet
 * representing same character in Roman-9 encoding.
 *
 * @type {Map<string, number>}
 */
export const toOctet = new Map( Array.from( table, ( code, octet ) => {
	return [ code === none ? String.fromCodePoint( 0xe000 + octet ) : String.fromCodePoint( code === same ? octet : code ), octet ];
} ) );

/**
 * Maps sequence of octets into string of characters based on HP's Roman-9
 * encoding.
 *
 * @param {Buffer} buffer sequence of octets assumed to represent string of characters in HP's Roman-9 encoding
 * @returns {string} string of characters according to octets in provided buffer
 */
export function fromBuffer( buffer ) {
	if ( !Buffer.isBuffer( buffer ) ) {
		throw new TypeError( "not a Buffer" );
	}

	let string = "";
	const size = buffer.length;

	for ( let i = 0; i < size; i++ ) {
		string += fromOctet.get( buffer[i] );
	}

	return string;
}

/**
 * Maps provided string into sequence of octets based on HP's Roman-9 encoding.
 *
 * @param {string} string sequence of characters to transcode
 * @returns {Buffer} sequence of octets representing provided characters according to Roman-9 encoding
 */
export function toBuffer( string ) {
	if ( typeof string !== "string" ) {
		throw new TypeError( "not a string" );
	}

	const size = string.length;
	const buffer = Buffer.alloc( size );

	for ( let i = 0; i < size; i++ ) {
		const code = toOctet.get( string[i] );

		buffer[i] = isNaN( code ) ? 0xff : code;
	}

	return buffer;
}
