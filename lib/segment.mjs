import { Command } from "./commands/index.mjs";

/**
 * Represents single PJL segment consisting of a sequence of PJL commands.
 *
 * @name Segment
 * @type {Segment}
 */
export class Segment {
	/** */
	constructor() {
		const commands = [];

		Object.defineProperties( this, {
			commands: { value: new Proxy( commands, {
				get: ( target, property ) => {
					switch ( property ) {
						case "push" : return ( ...newCommands ) => {
							for ( const command of newCommands ) {
								if ( !( command instanceof Command ) ) {
									throw new Error( "may add commands, only" );
								}
							}

							return commands.push( ...newCommands );
						};

						case "unshift" : return ( ...newCommands ) => {
							for ( const command of newCommands ) {
								if ( !( command instanceof Command ) ) {
									throw new Error( "may add commands, only" );
								}
							}

							return commands.unshift( ...newCommands );
						};

						case "splice" : return ( start, end, ...newCommands ) => {
							for ( const command of newCommands ) {
								if ( !( command instanceof Command ) ) {
									throw new Error( "may add commands, only" );
								}
							}

							commands.splice( start, end, ...newCommands );
						};

						default : return target[property];
					}
				},
				set: ( target, property, value ) => {
					if ( value instanceof Command && /^\d+$/.test( property ) && parseInt( property ) <= target.length ) {
						if ( value !== target[property] ) {
							target[property] = value; // eslint-disable-line no-param-reassign
						}

						return true;
					}

					return false;
				},
			} ) },
		} );
	}

	/**
	 * Converts current job into code complying with PJL syntax.
	 *
	 * @returns {string} source code of current PJL job
	 */
	toString() {
		return this.commands.map( command => String( command ) ).join( "" );
	}
}
