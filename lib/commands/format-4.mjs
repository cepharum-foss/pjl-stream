import { Command } from "./base.mjs";
import { Option } from "../option.mjs";

/**
 * Implements basic representation of a PJL command in format #3 according to
 * technical specs which is a command with a name and a sequence of words.
 *
 * @property {string} name name of represented command
 * @property {Option} modifier optional command modifier
 * @property {Option[]} options command options
 */
export class CommandWithOptions extends Command {
	/**
	 * @param {string} raw command's source as extracted from a PJL segment
	 * @param {string} name name of represented command
	 * @param {Option} modifier option used as command's modifier
	 * @param {Option[]} options command's options
	 */
	constructor( raw, name, modifier = null, options = null ) {
		super( raw );

		name = this.normalizeName( name ); // eslint-disable-line no-param-reassign
		modifier = this.normalizeModifier( modifier ); // eslint-disable-line no-param-reassign
		options = this.normalizeOptions( options ); // eslint-disable-line no-param-reassign

		Object.defineProperties( this, {
			name: {
				get() { return name.toUpperCase(); },
				set( value ) {
					const newName = this.normalizeName( value );

					if ( newName !== name ) {
						name = newName; // eslint-disable-line no-param-reassign
						this.raw = false;
					}
				},
			},

			modifier: {
				get: () => modifier,
				set( option ) {
					const normalized = this.normalizeModifier( option );

					if ( normalized !== modifier ) {
						modifier = normalized; // eslint-disable-line no-param-reassign
						this.raw = false;
					}
				},
			},

			options: { value: new Proxy( options, {
				get: ( target, property ) => {
					switch ( property ) {
						case "push" : return ( ...newOptions ) => {
							this.normalizeOptions( newOptions );

							this.raw = false;
							return options.push( ...newOptions );
						};

						case "pop" : return ( ...args ) => {
							if ( options.length > 0 ) {
								this.raw = false;
							}

							return options.pop( ...args );
						};

						case "unshift" : return ( ...newOptions ) => {
							this.normalizeOptions( newOptions );

							this.raw = false;
							return options.unshift( ...newOptions );
						};

						case "shift" : return ( ...args ) => {
							if ( options.length > 0 ) {
								this.raw = false;
							}

							return options.shift( ...args );
						};

						case "splice" : return ( start, end, ...newOptions ) => {
							this.normalizeOptions( newOptions );

							this.raw = false;
							options.splice( start, end, ...newOptions );
						};

						default : return target[property];
					}
				},
				set: ( target, property, value ) => {
					if ( value instanceof Option && /^\d+$/.test( property ) && parseInt( property ) <= target.length ) {
						if ( value !== target[property] ) {
							this.raw = false;
							target[property] = value; // eslint-disable-line no-param-reassign
						}

						return true;
					}

					return false;
				},
			} ) },

			_changed: {
				get: () => {
					if ( modifier?.changed ) {
						return true;
					}

					for ( const option of options ) {
						if ( option.changed ) {
							return true;
						}
					}

					return false;
				},
			},
		} );
	}

	/**
	 * Normalizes and validates command's name.
	 *
	 * @param {any} value new name of command to be normalized and validated
	 * @returns {string} normalized and validated command name
	 * @throws TypeError if validation fails
	 * @protected
	 */
	normalizeName( value ) {
		const asString = String( value == null ? "" : value ).trim().toUpperCase();

		if ( value == null || asString.length < 1 || /\s/.test( asString ) || [ "ECHO", "COMMENT" ].includes( asString ) ) {
			throw new TypeError( "invalid command name" );
		}

		return asString;
	}

	/**
	 * Normalizes and validates command's modifier.
	 *
	 * @param {Option?} modifier new modifier of command
	 * @returns {Option|null} normalized and validated command modifier
	 * @throws TypeError if validation fails
	 * @protected
	 */
	normalizeModifier( modifier ) {
		if ( modifier == null ) {
			return null;
		}

		if ( modifier instanceof Option ) {
			return modifier;
		}

		throw new TypeError( "invalid command modifier" );
	}

	/**
	 * Normalizes and validates list of command's options.
	 *
	 * @param {Option[]|null} options new list of command's options
	 * @returns {Option|null} normalized and validated list of command options
	 * @throws TypeError if validation fails
	 * @protected
	 */
	normalizeOptions( options ) {
		if ( options == null ) {
			return [];
		}

		if ( Array.isArray( options ) && options.every( option => option instanceof Option ) ) {
			return options;
		}

		throw new TypeError( "invalid list of command options" );
	}

	/** @inheritDoc */
	toString() {
		const changed = this.raw === false || this.modifier?.changed || this.options.some( option => option.changed );

		if ( !changed ) {
			return this.raw;
		}

		const parts = [this.name];

		if ( this.modifier ) {
			parts.push( this.modifier.render( ":" ) );
		}

		for ( const option of this.options ) {
			parts.push( option.render( "=" ) );
		}

		return `@PJL ${parts.join( " " )}\r\n`;
	}
}
