import { Command } from "./base.mjs";

/**
 * Implements basic representation of a PJL command in format #3 according to
 * technical specs which is a command with a name and a sequence of words.
 *
 * @property {string} name name of represented command
 * @property {string[]} words list of words following command's name
 */
export class CommandWithWords extends Command {
	/**
	 * @param {string} raw command's source as extracted from a PJL segment
	 * @param {string} name name of represented command
	 * @param {string} words words or remarks following command's name
	 */
	constructor( raw, name, words ) {
		super( raw );

		name = this.normalizeName( name ); // eslint-disable-line no-param-reassign
		words = this.normalizeWords( words ); // eslint-disable-line no-param-reassign

		Object.defineProperties( this, {
			name: {
				get() { return name; },
				set( value ) {
					value = this.normalizeName( value ); // eslint-disable-line no-param-reassign

					if ( value !== name ) {
						name = value; // eslint-disable-line no-param-reassign
						this.raw = false;
					}
				},
			},

			words: {
				get() { return words; },
				set( value ) {
					value = this.normalizeWords( value ); // eslint-disable-line no-param-reassign

					if ( value.join( "," ) !== words.join( "," ) ) {
						words = value; // eslint-disable-line no-param-reassign
						this.raw = false;
					}
				},
			},
		} );
	}

	/**
	 * Normalizes command name.
	 *
	 * @param {*} name stringifiable name
	 * @returns {string} normalized name
	 * @throws TypeError if resulting name is invalid
	 * @protected
	 */
	normalizeName( name ) {
		name = String( name == null ? "" : name ).trim().toUpperCase(); // eslint-disable-line no-param-reassign

		if ( !/^(?:COMMENT|ECHO)$/.test( name ) ) {
			throw new TypeError( "invalid command name" );
		}

		return name;
	}

	/**
	 * Normalizes command words.
	 *
	 * @param {*} words stringifiable list of words
	 * @returns {string[]} normalized list of words
	 * @throws TypeError if provided list of words is invalid
	 * @protected
	 */
	normalizeWords( words ) {
		words = Array.isArray( words ) ? words : String( words == null ? "" : words ).trim(); // eslint-disable-line no-param-reassign

		if ( typeof words === "string" ) {
			if ( words.length < 1 ) {
				throw new TypeError( `words/remarks of command ${this.name} must start with printable` );
			}

			words = words.split( /\s+/ ); // eslint-disable-line no-param-reassign
		}

		if ( !words || !Array.isArray( words ) || !words.length || !words.every( word => typeof word === "string" && word.trim().length > 0 ) ) {
			throw new TypeError( "invalid list of words/remarks" );
		}

		return words;
	}

	/** @inheritDoc */
	toString() {
		return this.raw ? this.raw : `@PJL ${this.name} ${this.words.join( " " )}\r\n`;
	}
}
