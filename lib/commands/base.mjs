/**
 * Represents single command of a PJL job.
 *
 * @property {string|false} raw command's source as extracted from a PJL segment, false if command has changed or source was missing on construction
 */
export class Command {
	/**
	 * @param {string} raw command's source as extracted from a PJL segment
	 */
	constructor( raw ) {
		if ( !raw ) {
			raw = false; // eslint-disable-line no-param-reassign
		} else if ( typeof raw !== "string" ) {
			raw = String( raw ); // eslint-disable-line no-param-reassign
		}

		Object.defineProperties( this, {
			raw: {
				get() { return this._changed ? false : raw; },
				set() {
					 raw = false; // eslint-disable-line no-param-reassign
				},
			},
		} );
	}
}
