import { Command } from "./base.mjs";

/**
 * Implements basic representation of a PJL command in format #2 according to
 * technical specs which is a line of PJL not representing any actual command
 * but some empty line supported for improving PJL readability.
 */
export class NopCommand extends Command {
	/**
	 * @param {string} raw raw code this command was parsed from
	 */
	constructor( raw = "" ) {
		super( raw );
	}

	/** @inheritDoc */
	toString() {
		return this.raw || "@PJL\r\n";
	}
}
