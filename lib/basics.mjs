/**
 * Represents format #1 PJL command which is used to escape from print data
 * stream for entering PJL language.
 *
 * @type {string}
 */
export const UEL = "\x1B%-12345X";
